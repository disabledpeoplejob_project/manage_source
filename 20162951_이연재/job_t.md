

```python
import pandas as pd
df=pd.read_csv('economy_t.csv',engine='python')
df=df[['직업별','2018. 12']]
df = df.drop([0,1,5,9],0)
df1 = df.set_index("직업별")
df = df.replace({'직업별': '1 관리자'}, {'직업별': '관리자'})
df = df.replace({'직업별': '2 전문가 및 관련 종사자'}, {'직업별': '전문가 및 관련 종사자'})
df = df.replace({'직업별': '3 사무 종사자'}, {'직업별': '사무 종사자'})
df = df.replace({'직업별': '4 서비스 종사자'}, {'직업별': '서비스 종사자'})
df = df.replace({'직업별': '5 판매 종사자'}, {'직업별': '판매 종사자'})
df = df.replace({'직업별': '6 농림어업 숙련 종사자'}, {'직업별': '농림어업 숙련 종사자'})
df = df.replace({'직업별': '7 기능원 및 관련 기능종사자'}, {'직업별': '기능원 및 관련 기능종사자'})
df = df.replace({'직업별': '8 장치,기계조작 및 조립종사자'}, {'직업별': '장치,기계조작 및 조립종사자'})
df = df.replace({'직업별': '9 단순노무 종사자'}, {'직업별': '단순노무 종사자'})
df1 = df.set_index("직업별")
import matplotlib.pyplot as plt
from matplotlib import font_manager, rc
import matplotlib
font_location="c:/Windows/fonts/malgun.ttf"
font_name=font_manager.FontProperties(fname=font_location).get_name()
matplotlib.rc('font',family=font_name)
df1.plot(kind='bar', title='취업자의 직종', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2250c70f6d8>




```python
df1.plot(kind='bar', title='취업자의 직종', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2250be71a58>




![output_2_1](/uploads/6f2256dda6dcb84ef1257d376177ea5f/output_2_1.png)



```python
df1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018. 12</th>
    </tr>
    <tr>
      <th>직업별</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>관리자</th>
      <td>414</td>
    </tr>
    <tr>
      <th>전문가 및 관련 종사자</th>
      <td>5511</td>
    </tr>
    <tr>
      <th>사무 종사자</th>
      <td>4765</td>
    </tr>
    <tr>
      <th>서비스 종사자</th>
      <td>2995</td>
    </tr>
    <tr>
      <th>판매 종사자</th>
      <td>3048</td>
    </tr>
    <tr>
      <th>농림어업 숙련 종사자</th>
      <td>1108</td>
    </tr>
    <tr>
      <th>기능원 및 관련 기능종사자</th>
      <td>2401</td>
    </tr>
    <tr>
      <th>장치,기계조작 및 조립종사자</th>
      <td>3047</td>
    </tr>
    <tr>
      <th>단순노무 종사자</th>
      <td>3350</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1=df1.sort_values(by=['직업별'],axis=0)
```


```python
type(df1)
```




    pandas.core.frame.DataFrame




```python
df1.plot(kind='bar', title='취업자의 직종', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2250d0377f0>




![output_5_1](/uploads/82a9dcbba00acaa1e33927d09abc0d47/output_5_1.png)



```python

```
