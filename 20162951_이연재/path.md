

```python
import pandas as pd
df=pd.read_csv('economy.csv',engine='python')
df.rename(columns={'D12_1':'취업자 입직경로'},inplace=True)
df=df[['취업자 입직경로']]
df = df.replace({'취업자 입직경로': ' '}, {'취업자 입직경로': '0'})
df = df.replace({'취업자 입직경로': '1'}, {'취업자 입직경로': '한국장애인고용공단'})
df = df.replace({'취업자 입직경로': '2'}, {'취업자 입직경로': '고용노동부 고용센터'})
df = df.replace({'취업자 입직경로': '3'}, {'취업자 입직경로': '지방자치단체'})
df = df.replace({'취업자 입직경로': '4'}, {'취업자 입직경로': '장애인복지관,직업재활시설'})
df = df.replace({'취업자 입직경로': '5'}, {'취업자 입직경로': '민관취업알선기관'})
df = df.replace({'취업자 입직경로': '6'}, {'취업자 입직경로': '대중매체'})
df = df.replace({'취업자 입직경로': '7'}, {'취업자 입직경로': '학교,학원'})
df = df.replace({'취업자 입직경로': '8'}, {'취업자 입직경로': '부모(친척),친구,동료 등 지인'})
df = df.replace({'취업자 입직경로': '9'}, {'취업자 입직경로': '기타'})
df1=df['취업자 입직경로'].value_counts()
df1.drop(['0'], inplace=True)
import matplotlib.pyplot as plt
plt.plot(df1)
from matplotlib import font_manager, rc
import matplotlib
font_location="c:/Windows/fonts/malgun.ttf"
font_name=font_manager.FontProperties(fname=font_location).get_name()
matplotlib.rc('font',family=font_name)
df1.plot(kind='bar', title='취업자 입직경로', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2155ea86ef0>




```python
df1.plot(kind='bar', title='취업자 입직경로', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2155e7a0208>




![path01](/uploads/2584b1303aefb39dc3a9b7c516023fdf/path01.png)



```python
df1=pd.DataFrame(df1)
```


```python
df1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>취업자 입직경로</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>부모(친척),친구,동료 등 지인</th>
      <td>1014</td>
    </tr>
    <tr>
      <th>장애인복지관,직업재활시설</th>
      <td>277</td>
    </tr>
    <tr>
      <th>대중매체</th>
      <td>258</td>
    </tr>
    <tr>
      <th>민관취업알선기관</th>
      <td>190</td>
    </tr>
    <tr>
      <th>지방자치단체</th>
      <td>181</td>
    </tr>
    <tr>
      <th>한국장애인고용공단</th>
      <td>103</td>
    </tr>
    <tr>
      <th>기타</th>
      <td>88</td>
    </tr>
    <tr>
      <th>고용노동부 고용센터</th>
      <td>81</td>
    </tr>
    <tr>
      <th>학교,학원</th>
      <td>66</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1.rename(columns={'취업자 입직경로':'2018'}, inplace = True)
```


```python
df1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>부모(친척),친구,동료 등 지인</th>
      <td>1014</td>
    </tr>
    <tr>
      <th>장애인복지관,직업재활시설</th>
      <td>277</td>
    </tr>
    <tr>
      <th>대중매체</th>
      <td>258</td>
    </tr>
    <tr>
      <th>민관취업알선기관</th>
      <td>190</td>
    </tr>
    <tr>
      <th>지방자치단체</th>
      <td>181</td>
    </tr>
    <tr>
      <th>한국장애인고용공단</th>
      <td>103</td>
    </tr>
    <tr>
      <th>기타</th>
      <td>88</td>
    </tr>
    <tr>
      <th>고용노동부 고용센터</th>
      <td>81</td>
    </tr>
    <tr>
      <th>학교,학원</th>
      <td>66</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1.index.name="취업자 입직경로"
```


```python
df1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
    <tr>
      <th>취업자 입직경로</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>부모(친척),친구,동료 등 지인</th>
      <td>1014</td>
    </tr>
    <tr>
      <th>장애인복지관,직업재활시설</th>
      <td>277</td>
    </tr>
    <tr>
      <th>대중매체</th>
      <td>258</td>
    </tr>
    <tr>
      <th>민관취업알선기관</th>
      <td>190</td>
    </tr>
    <tr>
      <th>지방자치단체</th>
      <td>181</td>
    </tr>
    <tr>
      <th>한국장애인고용공단</th>
      <td>103</td>
    </tr>
    <tr>
      <th>기타</th>
      <td>88</td>
    </tr>
    <tr>
      <th>고용노동부 고용센터</th>
      <td>81</td>
    </tr>
    <tr>
      <th>학교,학원</th>
      <td>66</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1.plot(kind='bar', title='취업자 입직경로', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2155e791128>




![path02](/uploads/1bf5a2973c04c988c5cfff0da725cc31/path02.png)



```python
df1=df1.sort_values(by=['2018'],axis=0,ascending=False)
```


```python
df1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
    <tr>
      <th>취업자 입직경로</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>학교,학원</th>
      <td>66</td>
    </tr>
    <tr>
      <th>고용노동부 고용센터</th>
      <td>81</td>
    </tr>
    <tr>
      <th>기타</th>
      <td>88</td>
    </tr>
    <tr>
      <th>한국장애인고용공단</th>
      <td>103</td>
    </tr>
    <tr>
      <th>지방자치단체</th>
      <td>181</td>
    </tr>
    <tr>
      <th>민관취업알선기관</th>
      <td>190</td>
    </tr>
    <tr>
      <th>대중매체</th>
      <td>258</td>
    </tr>
    <tr>
      <th>장애인복지관,직업재활시설</th>
      <td>277</td>
    </tr>
    <tr>
      <th>부모(친척),친구,동료 등 지인</th>
      <td>1014</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1.plot(kind='bar', title='취업자 입직경로', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2155eeeceb8>




![path03](/uploads/6e2945819407fa3dba8683e84ae55edd/path03.png)



```python
df1=df1.sort_values(by=['2018'],axis=0,ascending=False)
```


```python
df1.plot(kind='bar', title='취업자 입직경로', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2155ede4e80>




![path04](/uploads/24c19e3c88149a721e614fd2f3b5fa58/path04.png)



```python

```
