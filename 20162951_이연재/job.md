

```python
import pandas as pd
import numpy as np
df=pd.read_csv('economy.csv',engine='python')
df.rename(columns={'D10_1CB':'취업자의 직종'},inplace=True)
df=df[['취업자의 직종']]
df = df.replace({'취업자의 직종': ' '}, {'취업자의 직종': '0'})
df = df.replace({'취업자의 직종': '1'}, {'취업자의 직종': '관리자'})
df = df.replace({'취업자의 직종': '2'}, {'취업자의 직종': '전문가 및 관련 종사자'})
df = df.replace({'취업자의 직종': '3'}, {'취업자의 직종': '사무 종사자'})
df = df.replace({'취업자의 직종': '4'}, {'취업자의 직종': '서비스 종사자'})
df = df.replace({'취업자의 직종': '5'}, {'취업자의 직종': '판매 종사자'})
df = df.replace({'취업자의 직종': '6'}, {'취업자의 직종': '농림어업 숙련 종사자'})
df = df.replace({'취업자의 직종': '9'}, {'취업자의 직종': '단순 노무 종사자'})
df = df.replace({'취업자의 직종': '7'}, {'취업자의 직종': '기능원 및 관련 기능 종사자'})
df = df.replace({'취업자의 직종': '8'}, {'취업자의 직종': '장치·기계조작 및 조립 종사자'})
df = df.replace({'취업자의 직종': '10'}, {'취업자의 직종': '군인'})
df2=df['취업자의 직종'].value_counts()
df2.drop(['0'], inplace=True)
import matplotlib.pyplot as plt
from matplotlib import font_manager, rc
import matplotlib
font_location="c:/Windows/fonts/malgun.ttf"
font_name=font_manager.FontProperties(fname=font_location).get_name()
matplotlib.rc('font',family=font_name)
```


```python
df2.plot(kind='bar', title='취업자의 직종', figsize=(15, 5), fontsize=10, color='red',alpha=0.5)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x27933567eb8>




![output_1_1](/uploads/f95bc3cb72a876267ca8dda92963d78b/output_1_1.png)



```python
df2
```




    단순 노무 종사자           941
    사무 종사자              480
    장치·기계조작 및 조립 종사자    408
    전문가 및 관련 종사자        297
    농림어업 숙련 종사자         253
    판매 종사자              242
    기능원 및 관련 기능 종사자     238
    서비스 종사자             221
    관리자                  29
    군인                    1
    Name: 취업자의 직종, dtype: int64




```python
df2=pd.DataFrame(df2)
```


```python
df2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>취업자의 직종</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>단순 노무 종사자</th>
      <td>941</td>
    </tr>
    <tr>
      <th>사무 종사자</th>
      <td>480</td>
    </tr>
    <tr>
      <th>장치·기계조작 및 조립 종사자</th>
      <td>408</td>
    </tr>
    <tr>
      <th>전문가 및 관련 종사자</th>
      <td>297</td>
    </tr>
    <tr>
      <th>농림어업 숙련 종사자</th>
      <td>253</td>
    </tr>
    <tr>
      <th>판매 종사자</th>
      <td>242</td>
    </tr>
    <tr>
      <th>기능원 및 관련 기능 종사자</th>
      <td>238</td>
    </tr>
    <tr>
      <th>서비스 종사자</th>
      <td>221</td>
    </tr>
    <tr>
      <th>관리자</th>
      <td>29</td>
    </tr>
    <tr>
      <th>군인</th>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2.rename(columns={'취업자의 직종':'2018'}, inplace = True)
```


```python
df2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>단순 노무 종사자</th>
      <td>941</td>
    </tr>
    <tr>
      <th>사무 종사자</th>
      <td>480</td>
    </tr>
    <tr>
      <th>장치·기계조작 및 조립 종사자</th>
      <td>408</td>
    </tr>
    <tr>
      <th>전문가 및 관련 종사자</th>
      <td>297</td>
    </tr>
    <tr>
      <th>농림어업 숙련 종사자</th>
      <td>253</td>
    </tr>
    <tr>
      <th>판매 종사자</th>
      <td>242</td>
    </tr>
    <tr>
      <th>기능원 및 관련 기능 종사자</th>
      <td>238</td>
    </tr>
    <tr>
      <th>서비스 종사자</th>
      <td>221</td>
    </tr>
    <tr>
      <th>관리자</th>
      <td>29</td>
    </tr>
    <tr>
      <th>군인</th>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2.index
```




    Index(['단순 노무 종사자', '사무 종사자', '장치·기계조작 및 조립 종사자', '전문가 및 관련 종사자',
           '농림어업 숙련 종사자', '판매 종사자', '기능원 및 관련 기능 종사자', '서비스 종사자', '관리자', '군인'],
          dtype='object')




```python
df2.columns
```




    Index(['2018'], dtype='object')




```python
df2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>단순 노무 종사자</th>
      <td>941</td>
    </tr>
    <tr>
      <th>사무 종사자</th>
      <td>480</td>
    </tr>
    <tr>
      <th>장치·기계조작 및 조립 종사자</th>
      <td>408</td>
    </tr>
    <tr>
      <th>전문가 및 관련 종사자</th>
      <td>297</td>
    </tr>
    <tr>
      <th>농림어업 숙련 종사자</th>
      <td>253</td>
    </tr>
    <tr>
      <th>판매 종사자</th>
      <td>242</td>
    </tr>
    <tr>
      <th>기능원 및 관련 기능 종사자</th>
      <td>238</td>
    </tr>
    <tr>
      <th>서비스 종사자</th>
      <td>221</td>
    </tr>
    <tr>
      <th>관리자</th>
      <td>29</td>
    </tr>
    <tr>
      <th>군인</th>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2.index.name="직업별"
df2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
    <tr>
      <th>직업별</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>단순 노무 종사자</th>
      <td>941</td>
    </tr>
    <tr>
      <th>사무 종사자</th>
      <td>480</td>
    </tr>
    <tr>
      <th>장치·기계조작 및 조립 종사자</th>
      <td>408</td>
    </tr>
    <tr>
      <th>전문가 및 관련 종사자</th>
      <td>297</td>
    </tr>
    <tr>
      <th>농림어업 숙련 종사자</th>
      <td>253</td>
    </tr>
    <tr>
      <th>판매 종사자</th>
      <td>242</td>
    </tr>
    <tr>
      <th>기능원 및 관련 기능 종사자</th>
      <td>238</td>
    </tr>
    <tr>
      <th>서비스 종사자</th>
      <td>221</td>
    </tr>
    <tr>
      <th>관리자</th>
      <td>29</td>
    </tr>
    <tr>
      <th>군인</th>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2.sort_values(by=['직업별'],axis=0)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
    <tr>
      <th>직업별</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>관리자</th>
      <td>29</td>
    </tr>
    <tr>
      <th>군인</th>
      <td>1</td>
    </tr>
    <tr>
      <th>기능원 및 관련 기능 종사자</th>
      <td>238</td>
    </tr>
    <tr>
      <th>농림어업 숙련 종사자</th>
      <td>253</td>
    </tr>
    <tr>
      <th>단순 노무 종사자</th>
      <td>941</td>
    </tr>
    <tr>
      <th>사무 종사자</th>
      <td>480</td>
    </tr>
    <tr>
      <th>서비스 종사자</th>
      <td>221</td>
    </tr>
    <tr>
      <th>장치·기계조작 및 조립 종사자</th>
      <td>408</td>
    </tr>
    <tr>
      <th>전문가 및 관련 종사자</th>
      <td>297</td>
    </tr>
    <tr>
      <th>판매 종사자</th>
      <td>242</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2.drop(['군인'], inplace=True)
```


```python
df2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
    <tr>
      <th>직업별</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>단순 노무 종사자</th>
      <td>941</td>
    </tr>
    <tr>
      <th>사무 종사자</th>
      <td>480</td>
    </tr>
    <tr>
      <th>장치·기계조작 및 조립 종사자</th>
      <td>408</td>
    </tr>
    <tr>
      <th>전문가 및 관련 종사자</th>
      <td>297</td>
    </tr>
    <tr>
      <th>농림어업 숙련 종사자</th>
      <td>253</td>
    </tr>
    <tr>
      <th>판매 종사자</th>
      <td>242</td>
    </tr>
    <tr>
      <th>기능원 및 관련 기능 종사자</th>
      <td>238</td>
    </tr>
    <tr>
      <th>서비스 종사자</th>
      <td>221</td>
    </tr>
    <tr>
      <th>관리자</th>
      <td>29</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2=df2.sort_values(by=['직업별'],axis=0)
```


```python
df2.plot(kind='bar', title='취업자의 직종', figsize=(15, 5), fontsize=10, color='red',alpha=0.5)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2793389c240>




![output_15_1](/uploads/7a52cc91c0f6f514c124456a6a2afb1d/output_15_1.png)



```python

```
