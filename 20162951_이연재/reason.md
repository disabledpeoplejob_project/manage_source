

```python
import pandas as pd
df=pd.read_csv('economy.csv',engine='python')
df.rename(columns={'E14_1':'실업상태지속 이유'},inplace=True)
df=df[['실업상태지속 이유']]
df = df.replace({'실업상태지속 이유': ' '}, {'실업상태지속 이유': '0'})
df = df.replace({'실업상태지속 이유': '1'}, {'실업상태지속 이유': '학력, 경력, 기술부족'})
df = df.replace({'실업상태지속 이유': '2'}, {'실업상태지속 이유': '수입이나 임금이 맞지 않아서'})
df = df.replace({'실업상태지속 이유': '3'}, {'실업상태지속 이유': '근무환경이나 근무시간 등이 맞지 않아서'})
df = df.replace({'실업상태지속 이유': '4'}, {'실업상태지속 이유': '구직 정보접근의 어려움'})
df = df.replace({'실업상태지속 이유': '5'}, {'실업상태지속 이유': '취업알선기관 및 서비스 부재, 접근의 어려움'})
df = df.replace({'실업상태지속 이유': '6'}, {'실업상태지속 이유': '장애인에 대한 차별과 선입견'})
df = df.replace({'실업상태지속 이유': '7'}, {'실업상태지속 이유': '나이가 너무 어리거나 많아서'})
df = df.replace({'실업상태지속 이유': '8'}, {'실업상태지속 이유': '장애 이외의 질병이나 사고'})
df = df.replace({'실업상태지속 이유': '9'}, {'실업상태지속 이유': '심리적 불안감이나 초조함'})
df = df.replace({'실업상태지속 이유': '10'}, {'실업상태지속 이유': '신체기능의 제한'})
df = df.replace({'실업상태지속 이유': '11'}, {'실업상태지속 이유': '아동능력의 제한'})
df = df.replace({'실업상태지속 이유': '12'}, {'실업상태지속 이유': '의사소통의 제한'})
df = df.replace({'실업상태지속 이유': '13'}, {'실업상태지속 이유': '기타'})
df1=df['실업상태지속 이유'].value_counts()
df1.drop(['0'], inplace=True)
import matplotlib.pyplot as plt
plt.plot(df1)
from matplotlib import font_manager, rc
import matplotlib
font_location="c:/Windows/fonts/malgun.ttf"
font_name=font_manager.FontProperties(fname=font_location).get_name()
matplotlib.rc('font',family=font_name)
df1.plot(kind='bar', title='실업상태지속 이유', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x20326756b70>




```python
df1.plot(kind='bar', title='실업상태지속 이유', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x20328bc7048>




![reason01](/uploads/3094cd09fb235ee1bc197cbd42b7801d/reason01.png)



```python
df1
```




    장애인에 대한 차별과 선입견             45
    구직 정보접근의 어려움                32
    근무환경이나 근무시간 등이 맞지 않아서       32
    나이가 너무 어리거나 많아서             30
    수입이나 임금이 맞지 않아서             21
    장애 이외의 질병이나 사고              20
    신체기능의 제한                    19
    학력, 경력, 기술부족                15
    취업알선기관 및 서비스 부재, 접근의 어려움    12
    심리적 불안감이나 초조함               12
    의사소통의 제한                     6
    Name: 실업상태지속 이유, dtype: int64




```python
df1=pd.DataFrame(df1)
```


```python
df1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>실업상태지속 이유</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>장애인에 대한 차별과 선입견</th>
      <td>45</td>
    </tr>
    <tr>
      <th>구직 정보접근의 어려움</th>
      <td>32</td>
    </tr>
    <tr>
      <th>근무환경이나 근무시간 등이 맞지 않아서</th>
      <td>32</td>
    </tr>
    <tr>
      <th>나이가 너무 어리거나 많아서</th>
      <td>30</td>
    </tr>
    <tr>
      <th>수입이나 임금이 맞지 않아서</th>
      <td>21</td>
    </tr>
    <tr>
      <th>장애 이외의 질병이나 사고</th>
      <td>20</td>
    </tr>
    <tr>
      <th>신체기능의 제한</th>
      <td>19</td>
    </tr>
    <tr>
      <th>학력, 경력, 기술부족</th>
      <td>15</td>
    </tr>
    <tr>
      <th>취업알선기관 및 서비스 부재, 접근의 어려움</th>
      <td>12</td>
    </tr>
    <tr>
      <th>심리적 불안감이나 초조함</th>
      <td>12</td>
    </tr>
    <tr>
      <th>의사소통의 제한</th>
      <td>6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1.rename(columns={'실업상태지속 이유':'2018'}, inplace = True)
```


```python
df1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>장애인에 대한 차별과 선입견</th>
      <td>45</td>
    </tr>
    <tr>
      <th>구직 정보접근의 어려움</th>
      <td>32</td>
    </tr>
    <tr>
      <th>근무환경이나 근무시간 등이 맞지 않아서</th>
      <td>32</td>
    </tr>
    <tr>
      <th>나이가 너무 어리거나 많아서</th>
      <td>30</td>
    </tr>
    <tr>
      <th>수입이나 임금이 맞지 않아서</th>
      <td>21</td>
    </tr>
    <tr>
      <th>장애 이외의 질병이나 사고</th>
      <td>20</td>
    </tr>
    <tr>
      <th>신체기능의 제한</th>
      <td>19</td>
    </tr>
    <tr>
      <th>학력, 경력, 기술부족</th>
      <td>15</td>
    </tr>
    <tr>
      <th>취업알선기관 및 서비스 부재, 접근의 어려움</th>
      <td>12</td>
    </tr>
    <tr>
      <th>심리적 불안감이나 초조함</th>
      <td>12</td>
    </tr>
    <tr>
      <th>의사소통의 제한</th>
      <td>6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1.index.name="실업상태지속 이유"
```


```python
df1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
    <tr>
      <th>실업상태지속 이유</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>장애인에 대한 차별과 선입견</th>
      <td>45</td>
    </tr>
    <tr>
      <th>구직 정보접근의 어려움</th>
      <td>32</td>
    </tr>
    <tr>
      <th>근무환경이나 근무시간 등이 맞지 않아서</th>
      <td>32</td>
    </tr>
    <tr>
      <th>나이가 너무 어리거나 많아서</th>
      <td>30</td>
    </tr>
    <tr>
      <th>수입이나 임금이 맞지 않아서</th>
      <td>21</td>
    </tr>
    <tr>
      <th>장애 이외의 질병이나 사고</th>
      <td>20</td>
    </tr>
    <tr>
      <th>신체기능의 제한</th>
      <td>19</td>
    </tr>
    <tr>
      <th>학력, 경력, 기술부족</th>
      <td>15</td>
    </tr>
    <tr>
      <th>취업알선기관 및 서비스 부재, 접근의 어려움</th>
      <td>12</td>
    </tr>
    <tr>
      <th>심리적 불안감이나 초조함</th>
      <td>12</td>
    </tr>
    <tr>
      <th>의사소통의 제한</th>
      <td>6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1=df1.sort_values(by=['2018'],axis=0,ascending=False)
```


```python
df1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2018</th>
    </tr>
    <tr>
      <th>실업상태지속 이유</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>장애인에 대한 차별과 선입견</th>
      <td>45</td>
    </tr>
    <tr>
      <th>구직 정보접근의 어려움</th>
      <td>32</td>
    </tr>
    <tr>
      <th>근무환경이나 근무시간 등이 맞지 않아서</th>
      <td>32</td>
    </tr>
    <tr>
      <th>나이가 너무 어리거나 많아서</th>
      <td>30</td>
    </tr>
    <tr>
      <th>수입이나 임금이 맞지 않아서</th>
      <td>21</td>
    </tr>
    <tr>
      <th>장애 이외의 질병이나 사고</th>
      <td>20</td>
    </tr>
    <tr>
      <th>신체기능의 제한</th>
      <td>19</td>
    </tr>
    <tr>
      <th>학력, 경력, 기술부족</th>
      <td>15</td>
    </tr>
    <tr>
      <th>취업알선기관 및 서비스 부재, 접근의 어려움</th>
      <td>12</td>
    </tr>
    <tr>
      <th>심리적 불안감이나 초조함</th>
      <td>12</td>
    </tr>
    <tr>
      <th>의사소통의 제한</th>
      <td>6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1.plot(kind='bar', title='실업상태지속 이유', figsize=(15, 5), fontsize=10)
```




    <matplotlib.axes._subplots.AxesSubplot at 0x20326060e80>




![reason02](/uploads/b691634b8b40855d56c654e379ab1962/reason02.png)



```python

```
