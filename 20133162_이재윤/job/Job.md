

```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import font_manager, rc
from matplotlib import style
import matplotlib
font_location="c:/windows/fonts/malgun.ttf"
font_name=font_manager.FontProperties(fname=font_location).get_name()
matplotlib.rc('font',family=font_name)
```


```python
df=pd.read_excel('현황.xlsx')
df1=pd.read_excel('지역별구인수.xlsx')
df2=pd.read_excel('이유.xlsx')
df3=pd.read_excel('이유.xlsx')
```


```python
df.head() 
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>장애등급별(1)</th>
      <th>2013 4/4</th>
      <th>2013 4/4.1</th>
      <th>2013 4/4.2</th>
      <th>2013 4/4.3</th>
      <th>2013 4/4.4</th>
      <th>2013 4/4.5</th>
      <th>2015 4/4</th>
      <th>2015 4/4.1</th>
      <th>2015 4/4.2</th>
      <th>2015 4/4.3</th>
      <th>2015 4/4.4</th>
      <th>2015 4/4.5</th>
      <th>2017 4/4</th>
      <th>2017 4/4.1</th>
      <th>2017 4/4.2</th>
      <th>2017 4/4.3</th>
      <th>2017 4/4.4</th>
      <th>2017 4/4.5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>장애등급별(1)</td>
      <td>구직자수 (명)</td>
      <td>전년도 동분기 구직자수 증감 (%)</td>
      <td>취업자수 (명)</td>
      <td>전년도 동분기 취업자수 증감 (%)</td>
      <td>취업률 (%)</td>
      <td>전년도 동분기 취업률 증감 (%p)</td>
      <td>구직자수 (명)</td>
      <td>전년도 동분기 구직자수 증감 (%)</td>
      <td>취업자수 (명)</td>
      <td>전년도 동분기 취업자수 증감 (%)</td>
      <td>취업률 (%)</td>
      <td>전년도 동분기 취업률 증감 (%p)</td>
      <td>구직자수 (명)</td>
      <td>전년도 동분기 구직자수 증감 (%)</td>
      <td>취업자수 (명)</td>
      <td>전년도 동분기 취업자수 증감 (%)</td>
      <td>취업률 (%)</td>
      <td>전년도 동분기 취업률 증감 (%p)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>합계</td>
      <td>10453</td>
      <td>0.3</td>
      <td>6249</td>
      <td>0.3</td>
      <td>59.8</td>
      <td>0</td>
      <td>10923</td>
      <td>17.5</td>
      <td>5962</td>
      <td>-19.1</td>
      <td>54.6</td>
      <td>-24.7</td>
      <td>14329</td>
      <td>23.7</td>
      <td>5859</td>
      <td>1.3</td>
      <td>40.9</td>
      <td>-9</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1급</td>
      <td>583</td>
      <td>8</td>
      <td>319</td>
      <td>11.1</td>
      <td>54.7</td>
      <td>1.6</td>
      <td>486</td>
      <td>6.1</td>
      <td>289</td>
      <td>-17.2</td>
      <td>59.5</td>
      <td>-16.7</td>
      <td>693</td>
      <td>26.5</td>
      <td>311</td>
      <td>13.9</td>
      <td>44.9</td>
      <td>-4.9</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2급</td>
      <td>2285</td>
      <td>-10.1</td>
      <td>1462</td>
      <td>-9.5</td>
      <td>64</td>
      <td>0.4</td>
      <td>2302</td>
      <td>16.7</td>
      <td>1338</td>
      <td>-13.7</td>
      <td>58.1</td>
      <td>-20.5</td>
      <td>2865</td>
      <td>27.5</td>
      <td>1325</td>
      <td>2.6</td>
      <td>46.2</td>
      <td>-11.3</td>
    </tr>
    <tr>
      <th>4</th>
      <td>3급</td>
      <td>3291</td>
      <td>-1.2</td>
      <td>1996</td>
      <td>-2.7</td>
      <td>60.7</td>
      <td>-0.9</td>
      <td>3541</td>
      <td>17.1</td>
      <td>2027</td>
      <td>-12.7</td>
      <td>57.2</td>
      <td>-19.5</td>
      <td>4608</td>
      <td>17.4</td>
      <td>2061</td>
      <td>2</td>
      <td>44.7</td>
      <td>-6.8</td>
    </tr>
  </tbody>
</table>
</div>




```python
df.columns  # df의 컬럼명
```




    Index(['장애등급별(1)', '2013 4/4', '2013 4/4.1', '2013 4/4.2', '2013 4/4.3',
           '2013 4/4.4', '2013 4/4.5', '2015 4/4', '2015 4/4.1', '2015 4/4.2',
           '2015 4/4.3', '2015 4/4.4', '2015 4/4.5', '2017 4/4', '2017 4/4.1',
           '2017 4/4.2', '2017 4/4.3', '2017 4/4.4', '2017 4/4.5'],
          dtype='object')




```python
df=df.rename(columns={'2013 4/4':'2013 구직자수','2013 4/4.2':'2013 취업자수',
                  '2015 4/4':'2015 구직자수','2015 4/4.2':'2015 취업자수',
                  '2017 4/4':'2017 구직자수','2017 4/4.2':'2017 취업자수'})   # 사용할 정보만 이름 변경
```


```python
df=df.drop(0)  # 0번째 행 삭제
```


```python
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>장애등급별(1)</th>
      <th>2013 구직자수</th>
      <th>2013 4/4.1</th>
      <th>2013 취업자수</th>
      <th>2013 4/4.3</th>
      <th>2013 4/4.4</th>
      <th>2013 4/4.5</th>
      <th>2015 구직자수</th>
      <th>2015 4/4.1</th>
      <th>2015 취업자수</th>
      <th>2015 4/4.3</th>
      <th>2015 4/4.4</th>
      <th>2015 4/4.5</th>
      <th>2017 구직자수</th>
      <th>2017 4/4.1</th>
      <th>2017 취업자수</th>
      <th>2017 4/4.3</th>
      <th>2017 4/4.4</th>
      <th>2017 4/4.5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>합계</td>
      <td>10453</td>
      <td>0.3</td>
      <td>6249</td>
      <td>0.3</td>
      <td>59.8</td>
      <td>0</td>
      <td>10923</td>
      <td>17.5</td>
      <td>5962</td>
      <td>-19.1</td>
      <td>54.6</td>
      <td>-24.7</td>
      <td>14329</td>
      <td>23.7</td>
      <td>5859</td>
      <td>1.3</td>
      <td>40.9</td>
      <td>-9</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1급</td>
      <td>583</td>
      <td>8</td>
      <td>319</td>
      <td>11.1</td>
      <td>54.7</td>
      <td>1.6</td>
      <td>486</td>
      <td>6.1</td>
      <td>289</td>
      <td>-17.2</td>
      <td>59.5</td>
      <td>-16.7</td>
      <td>693</td>
      <td>26.5</td>
      <td>311</td>
      <td>13.9</td>
      <td>44.9</td>
      <td>-4.9</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2급</td>
      <td>2285</td>
      <td>-10.1</td>
      <td>1462</td>
      <td>-9.5</td>
      <td>64</td>
      <td>0.4</td>
      <td>2302</td>
      <td>16.7</td>
      <td>1338</td>
      <td>-13.7</td>
      <td>58.1</td>
      <td>-20.5</td>
      <td>2865</td>
      <td>27.5</td>
      <td>1325</td>
      <td>2.6</td>
      <td>46.2</td>
      <td>-11.3</td>
    </tr>
    <tr>
      <th>4</th>
      <td>3급</td>
      <td>3291</td>
      <td>-1.2</td>
      <td>1996</td>
      <td>-2.7</td>
      <td>60.7</td>
      <td>-0.9</td>
      <td>3541</td>
      <td>17.1</td>
      <td>2027</td>
      <td>-12.7</td>
      <td>57.2</td>
      <td>-19.5</td>
      <td>4608</td>
      <td>17.4</td>
      <td>2061</td>
      <td>2</td>
      <td>44.7</td>
      <td>-6.8</td>
    </tr>
    <tr>
      <th>5</th>
      <td>4급</td>
      <td>1111</td>
      <td>-0.8</td>
      <td>584</td>
      <td>-3.9</td>
      <td>52.6</td>
      <td>-1.7</td>
      <td>1249</td>
      <td>20.8</td>
      <td>584</td>
      <td>-27.7</td>
      <td>46.8</td>
      <td>-31.3</td>
      <td>1510</td>
      <td>17.1</td>
      <td>510</td>
      <td>-6.9</td>
      <td>33.8</td>
      <td>-8.7</td>
    </tr>
    <tr>
      <th>6</th>
      <td>5급</td>
      <td>1322</td>
      <td>8.1</td>
      <td>772</td>
      <td>11.7</td>
      <td>58.4</td>
      <td>1.9</td>
      <td>1430</td>
      <td>22.2</td>
      <td>722</td>
      <td>-26.6</td>
      <td>50.5</td>
      <td>-33.5</td>
      <td>1959</td>
      <td>27.2</td>
      <td>655</td>
      <td>-5.2</td>
      <td>33.4</td>
      <td>-11.5</td>
    </tr>
    <tr>
      <th>7</th>
      <td>6급</td>
      <td>1829</td>
      <td>13.5</td>
      <td>1080</td>
      <td>15.5</td>
      <td>59</td>
      <td>1</td>
      <td>1876</td>
      <td>16.4</td>
      <td>970</td>
      <td>-27.2</td>
      <td>51.7</td>
      <td>-31</td>
      <td>2661</td>
      <td>32.2</td>
      <td>974</td>
      <td>3.5</td>
      <td>36.6</td>
      <td>-10.1</td>
    </tr>
    <tr>
      <th>8</th>
      <td>7급</td>
      <td>32</td>
      <td>-37.3</td>
      <td>36</td>
      <td>-18.2</td>
      <td>112.5</td>
      <td>26.2</td>
      <td>39</td>
      <td>62.5</td>
      <td>32</td>
      <td>18.5</td>
      <td>82.1</td>
      <td>-30.4</td>
      <td>33</td>
      <td>43.5</td>
      <td>23</td>
      <td>43.8</td>
      <td>69.7</td>
      <td>0.1</td>
    </tr>
    <tr>
      <th>9</th>
      <td>미분류</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
      <td>-</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1.head()  
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>지역별(1)</th>
      <th>2013 4/4</th>
      <th>2013 4/4.1</th>
      <th>2013 4/4.2</th>
      <th>2013 4/4.3</th>
      <th>2013 4/4.4</th>
      <th>2013 4/4.5</th>
      <th>2015 4/4</th>
      <th>2015 4/4.1</th>
      <th>2015 4/4.2</th>
      <th>...</th>
      <th>2017 4/4.2</th>
      <th>2017 4/4.3</th>
      <th>2017 4/4.4</th>
      <th>2017 4/4.5</th>
      <th>2018 4/4</th>
      <th>2018 4/4.1</th>
      <th>2018 4/4.2</th>
      <th>2018 4/4.3</th>
      <th>2018 4/4.4</th>
      <th>2018 4/4.5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>지역별(1)</td>
      <td>구인수 (명)</td>
      <td>전년도 동분기 구인수 증감 (%)</td>
      <td>구직자수 (명)</td>
      <td>전년도 동분기 구직자수 증감 (%)</td>
      <td>구인배수 (명)</td>
      <td>전년도 동분기 구인배수 증감 (%)</td>
      <td>구인수 (명)</td>
      <td>전년도 동분기 구인수 증감 (%)</td>
      <td>구직자수 (명)</td>
      <td>...</td>
      <td>구직자수 (명)</td>
      <td>전년도 동분기 구직자수 증감 (%)</td>
      <td>구인배수 (명)</td>
      <td>전년도 동분기 구인배수 증감 (%)</td>
      <td>구인수 (명)</td>
      <td>전년도 동분기 구인수 증감 (%)</td>
      <td>구직자수 (명)</td>
      <td>전년도 동분기 구직자수 증감 (%)</td>
      <td>구인배수 (명)</td>
      <td>전년도 동분기 구인배수 증감 (%)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>합계</td>
      <td>20080</td>
      <td>14.8</td>
      <td>10453</td>
      <td>0.3</td>
      <td>1.9</td>
      <td>0.2</td>
      <td>17748</td>
      <td>20.9</td>
      <td>10923</td>
      <td>...</td>
      <td>14329</td>
      <td>23.7</td>
      <td>1.3</td>
      <td>-0.2</td>
      <td>20570</td>
      <td>6.8</td>
      <td>16501</td>
      <td>15.2</td>
      <td>1.2</td>
      <td>-0.1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>서울</td>
      <td>7057</td>
      <td>39.5</td>
      <td>1861</td>
      <td>-4.4</td>
      <td>3.8</td>
      <td>1.2</td>
      <td>4304</td>
      <td>-25.2</td>
      <td>1657</td>
      <td>...</td>
      <td>2191</td>
      <td>28</td>
      <td>2.4</td>
      <td>-0.7</td>
      <td>5370</td>
      <td>1.1</td>
      <td>2127</td>
      <td>-2.9</td>
      <td>2.5</td>
      <td>0.1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>경기</td>
      <td>3634</td>
      <td>14.9</td>
      <td>2546</td>
      <td>14.7</td>
      <td>1.4</td>
      <td>0</td>
      <td>4304</td>
      <td>98.6</td>
      <td>3490</td>
      <td>...</td>
      <td>3710</td>
      <td>5.5</td>
      <td>1.3</td>
      <td>0</td>
      <td>4862</td>
      <td>0.9</td>
      <td>4059</td>
      <td>9.4</td>
      <td>1.2</td>
      <td>-0.1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>인천</td>
      <td>762</td>
      <td>-26.2</td>
      <td>612</td>
      <td>-12.4</td>
      <td>1.2</td>
      <td>-0.3</td>
      <td>850</td>
      <td>102.9</td>
      <td>598</td>
      <td>...</td>
      <td>899</td>
      <td>29</td>
      <td>1</td>
      <td>0.2</td>
      <td>1018</td>
      <td>11.3</td>
      <td>978</td>
      <td>8.8</td>
      <td>1</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 25 columns</p>
</div>




```python
df1.columns  # df1의 컬럼명 확인
```




    Index(['지역별(1)', '2013 4/4', '2013 4/4.1', '2013 4/4.2', '2013 4/4.3',
           '2013 4/4.4', '2013 4/4.5', '2015 4/4', '2015 4/4.1', '2015 4/4.2',
           '2015 4/4.3', '2015 4/4.4', '2015 4/4.5', '2017 4/4', '2017 4/4.1',
           '2017 4/4.2', '2017 4/4.3', '2017 4/4.4', '2017 4/4.5', '2018 4/4',
           '2018 4/4.1', '2018 4/4.2', '2018 4/4.3', '2018 4/4.4', '2018 4/4.5'],
          dtype='object')




```python
df1=df1.rename(columns={'지역별(1)':'지역','2013 4/4':'2013 구인수','2013 4/4.2':'2013 구직자수',
                   '2015 4/4':'2015 구인수','2015 4/4.2':'2015 구직자수',
                   '2017 4/4':'2017 구인수','2017 4/4.2':'2017 구직자수'})  # 사용할 정보만 이름변경
```


```python
df1.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>지역</th>
      <th>2013 구인수</th>
      <th>2013 4/4.1</th>
      <th>2013 구직자수</th>
      <th>2013 4/4.3</th>
      <th>2013 4/4.4</th>
      <th>2013 4/4.5</th>
      <th>2015 구인수</th>
      <th>2015 4/4.1</th>
      <th>2015 구직자수</th>
      <th>...</th>
      <th>2017 구직자수</th>
      <th>2017 4/4.3</th>
      <th>2017 4/4.4</th>
      <th>2017 4/4.5</th>
      <th>2018 4/4</th>
      <th>2018 4/4.1</th>
      <th>2018 4/4.2</th>
      <th>2018 4/4.3</th>
      <th>2018 4/4.4</th>
      <th>2018 4/4.5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>지역별(1)</td>
      <td>구인수 (명)</td>
      <td>전년도 동분기 구인수 증감 (%)</td>
      <td>구직자수 (명)</td>
      <td>전년도 동분기 구직자수 증감 (%)</td>
      <td>구인배수 (명)</td>
      <td>전년도 동분기 구인배수 증감 (%)</td>
      <td>구인수 (명)</td>
      <td>전년도 동분기 구인수 증감 (%)</td>
      <td>구직자수 (명)</td>
      <td>...</td>
      <td>구직자수 (명)</td>
      <td>전년도 동분기 구직자수 증감 (%)</td>
      <td>구인배수 (명)</td>
      <td>전년도 동분기 구인배수 증감 (%)</td>
      <td>구인수 (명)</td>
      <td>전년도 동분기 구인수 증감 (%)</td>
      <td>구직자수 (명)</td>
      <td>전년도 동분기 구직자수 증감 (%)</td>
      <td>구인배수 (명)</td>
      <td>전년도 동분기 구인배수 증감 (%)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>합계</td>
      <td>20080</td>
      <td>14.8</td>
      <td>10453</td>
      <td>0.3</td>
      <td>1.9</td>
      <td>0.2</td>
      <td>17748</td>
      <td>20.9</td>
      <td>10923</td>
      <td>...</td>
      <td>14329</td>
      <td>23.7</td>
      <td>1.3</td>
      <td>-0.2</td>
      <td>20570</td>
      <td>6.8</td>
      <td>16501</td>
      <td>15.2</td>
      <td>1.2</td>
      <td>-0.1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>서울</td>
      <td>7057</td>
      <td>39.5</td>
      <td>1861</td>
      <td>-4.4</td>
      <td>3.8</td>
      <td>1.2</td>
      <td>4304</td>
      <td>-25.2</td>
      <td>1657</td>
      <td>...</td>
      <td>2191</td>
      <td>28</td>
      <td>2.4</td>
      <td>-0.7</td>
      <td>5370</td>
      <td>1.1</td>
      <td>2127</td>
      <td>-2.9</td>
      <td>2.5</td>
      <td>0.1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>경기</td>
      <td>3634</td>
      <td>14.9</td>
      <td>2546</td>
      <td>14.7</td>
      <td>1.4</td>
      <td>0</td>
      <td>4304</td>
      <td>98.6</td>
      <td>3490</td>
      <td>...</td>
      <td>3710</td>
      <td>5.5</td>
      <td>1.3</td>
      <td>0</td>
      <td>4862</td>
      <td>0.9</td>
      <td>4059</td>
      <td>9.4</td>
      <td>1.2</td>
      <td>-0.1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>인천</td>
      <td>762</td>
      <td>-26.2</td>
      <td>612</td>
      <td>-12.4</td>
      <td>1.2</td>
      <td>-0.3</td>
      <td>850</td>
      <td>102.9</td>
      <td>598</td>
      <td>...</td>
      <td>899</td>
      <td>29</td>
      <td>1</td>
      <td>0.2</td>
      <td>1018</td>
      <td>11.3</td>
      <td>978</td>
      <td>8.8</td>
      <td>1</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 25 columns</p>
</div>




```python
df1=df1.drop([0])  #0번행 삭제
```


```python
df1.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>지역</th>
      <th>2013 구인수</th>
      <th>2013 4/4.1</th>
      <th>2013 구직자수</th>
      <th>2013 4/4.3</th>
      <th>2013 4/4.4</th>
      <th>2013 4/4.5</th>
      <th>2015 구인수</th>
      <th>2015 4/4.1</th>
      <th>2015 구직자수</th>
      <th>...</th>
      <th>2017 구직자수</th>
      <th>2017 4/4.3</th>
      <th>2017 4/4.4</th>
      <th>2017 4/4.5</th>
      <th>2018 4/4</th>
      <th>2018 4/4.1</th>
      <th>2018 4/4.2</th>
      <th>2018 4/4.3</th>
      <th>2018 4/4.4</th>
      <th>2018 4/4.5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>합계</td>
      <td>20080</td>
      <td>14.8</td>
      <td>10453</td>
      <td>0.3</td>
      <td>1.9</td>
      <td>0.2</td>
      <td>17748</td>
      <td>20.9</td>
      <td>10923</td>
      <td>...</td>
      <td>14329</td>
      <td>23.7</td>
      <td>1.3</td>
      <td>-0.2</td>
      <td>20570</td>
      <td>6.8</td>
      <td>16501</td>
      <td>15.2</td>
      <td>1.2</td>
      <td>-0.1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>서울</td>
      <td>7057</td>
      <td>39.5</td>
      <td>1861</td>
      <td>-4.4</td>
      <td>3.8</td>
      <td>1.2</td>
      <td>4304</td>
      <td>-25.2</td>
      <td>1657</td>
      <td>...</td>
      <td>2191</td>
      <td>28</td>
      <td>2.4</td>
      <td>-0.7</td>
      <td>5370</td>
      <td>1.1</td>
      <td>2127</td>
      <td>-2.9</td>
      <td>2.5</td>
      <td>0.1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>경기</td>
      <td>3634</td>
      <td>14.9</td>
      <td>2546</td>
      <td>14.7</td>
      <td>1.4</td>
      <td>0</td>
      <td>4304</td>
      <td>98.6</td>
      <td>3490</td>
      <td>...</td>
      <td>3710</td>
      <td>5.5</td>
      <td>1.3</td>
      <td>0</td>
      <td>4862</td>
      <td>0.9</td>
      <td>4059</td>
      <td>9.4</td>
      <td>1.2</td>
      <td>-0.1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>인천</td>
      <td>762</td>
      <td>-26.2</td>
      <td>612</td>
      <td>-12.4</td>
      <td>1.2</td>
      <td>-0.3</td>
      <td>850</td>
      <td>102.9</td>
      <td>598</td>
      <td>...</td>
      <td>899</td>
      <td>29</td>
      <td>1</td>
      <td>0.2</td>
      <td>1018</td>
      <td>11.3</td>
      <td>978</td>
      <td>8.8</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>강원</td>
      <td>624</td>
      <td>37.4</td>
      <td>284</td>
      <td>0</td>
      <td>2.2</td>
      <td>0.6</td>
      <td>502</td>
      <td>50.3</td>
      <td>234</td>
      <td>...</td>
      <td>429</td>
      <td>73.7</td>
      <td>1.8</td>
      <td>-0.7</td>
      <td>527</td>
      <td>-31.6</td>
      <td>415</td>
      <td>-3.3</td>
      <td>1.3</td>
      <td>-0.5</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 25 columns</p>
</div>




```python
df1=df1.head(1)  #합계만 사용하기 때문에 1번행만 사용
```


```python
df=df.head(1)    #
```


```python
df1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>지역</th>
      <th>2013 구인수</th>
      <th>2013 4/4.1</th>
      <th>2013 구직자수</th>
      <th>2013 4/4.3</th>
      <th>2013 4/4.4</th>
      <th>2013 4/4.5</th>
      <th>2015 구인수</th>
      <th>2015 4/4.1</th>
      <th>2015 구직자수</th>
      <th>...</th>
      <th>2017 구직자수</th>
      <th>2017 4/4.3</th>
      <th>2017 4/4.4</th>
      <th>2017 4/4.5</th>
      <th>2018 4/4</th>
      <th>2018 4/4.1</th>
      <th>2018 4/4.2</th>
      <th>2018 4/4.3</th>
      <th>2018 4/4.4</th>
      <th>2018 4/4.5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>합계</td>
      <td>20080</td>
      <td>14.8</td>
      <td>10453</td>
      <td>0.3</td>
      <td>1.9</td>
      <td>0.2</td>
      <td>17748</td>
      <td>20.9</td>
      <td>10923</td>
      <td>...</td>
      <td>14329</td>
      <td>23.7</td>
      <td>1.3</td>
      <td>-0.2</td>
      <td>20570</td>
      <td>6.8</td>
      <td>16501</td>
      <td>15.2</td>
      <td>1.2</td>
      <td>-0.1</td>
    </tr>
  </tbody>
</table>
<p>1 rows × 25 columns</p>
</div>




```python
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>장애등급별(1)</th>
      <th>2013 구직자수</th>
      <th>2013 4/4.1</th>
      <th>2013 취업자수</th>
      <th>2013 4/4.3</th>
      <th>2013 4/4.4</th>
      <th>2013 4/4.5</th>
      <th>2015 구직자수</th>
      <th>2015 4/4.1</th>
      <th>2015 취업자수</th>
      <th>2015 4/4.3</th>
      <th>2015 4/4.4</th>
      <th>2015 4/4.5</th>
      <th>2017 구직자수</th>
      <th>2017 4/4.1</th>
      <th>2017 취업자수</th>
      <th>2017 4/4.3</th>
      <th>2017 4/4.4</th>
      <th>2017 4/4.5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>합계</td>
      <td>10453</td>
      <td>0.3</td>
      <td>6249</td>
      <td>0.3</td>
      <td>59.8</td>
      <td>0</td>
      <td>10923</td>
      <td>17.5</td>
      <td>5962</td>
      <td>-19.1</td>
      <td>54.6</td>
      <td>-24.7</td>
      <td>14329</td>
      <td>23.7</td>
      <td>5859</td>
      <td>1.3</td>
      <td>40.9</td>
      <td>-9</td>
    </tr>
  </tbody>
</table>
</div>




```python
b13=df1[['2013 구인수','2013 구직자수']]  #df1의  구인수와 구직자수를  사용
b15=df1[['2015 구인수','2015 구직자수']]
b17=df1[['2017 구인수','2017 구직자수']]
```


```python
b13=b13.reset_index()  #인덱스 리셋
b15=b15.reset_index()
b17=b17.reset_index()
```


```python
b13
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>2013 구인수</th>
      <th>2013 구직자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>20080</td>
      <td>10453</td>
    </tr>
  </tbody>
</table>
</div>




```python
b15
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>2015 구인수</th>
      <th>2015 구직자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>17748</td>
      <td>10923</td>
    </tr>
  </tbody>
</table>
</div>




```python
b17
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>2017 구인수</th>
      <th>2017 구직자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>19261</td>
      <td>14329</td>
    </tr>
  </tbody>
</table>
</div>




```python
a13=df['2013 취업자수']  # 년도별 취업자수
a15=df['2015 취업자수']
a17=df['2017 취업자수']
```


```python
a13=pd.DataFrame(a13) # 데이터프레임 생성
a15=pd.DataFrame(a15)
a17=pd.DataFrame(a17)
```


```python
a13
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2013 취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>6249</td>
    </tr>
  </tbody>
</table>
</div>




```python
a15
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2015 취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>5962</td>
    </tr>
  </tbody>
</table>
</div>




```python
a17
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2017 취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>5859</td>
    </tr>
  </tbody>
</table>
</div>




```python
a13=a13.reset_index()  #인덱스 리셋
a15=a15.reset_index()
a17=a17.reset_index()
```


```python
df13=pd.merge(b13,a13,how='outer')   # 년도별 a와 b를 합침
df15=pd.merge(b15,a15,how='outer')
df17=pd.merge(b17,a17,how='outer')
```


```python
df13
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>2013 구인수</th>
      <th>2013 구직자수</th>
      <th>2013 취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>20080</td>
      <td>10453</td>
      <td>6249</td>
    </tr>
  </tbody>
</table>
</div>




```python
df15
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>2015 구인수</th>
      <th>2015 구직자수</th>
      <th>2015 취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>17748</td>
      <td>10923</td>
      <td>5962</td>
    </tr>
  </tbody>
</table>
</div>




```python
df17
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>2017 구인수</th>
      <th>2017 구직자수</th>
      <th>2017 취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>19261</td>
      <td>14329</td>
      <td>5859</td>
    </tr>
  </tbody>
</table>
</div>




```python
df13=df13.rename(columns={'2013 취업자수':'취업자수','2013 취업률':'취업률','2013 구인수':'구인수','2013 구직자수':'구직자수'})
df15=df15.rename(columns={'2015 취업자수':'취업자수','2015 취업률':'취업률','2015 구인수':'구인수','2015 구직자수':'구직자수'})
df17=df17.rename(columns={'2017 취업자수':'취업자수','2017 취업률':'취업률','2017 구인수':'구인수','2017 구직자수':'구직자수'})
# 컬럼명 변경
```


```python
df13=df13[['구인수','구직자수','취업자수']]  #필요한 정보만 사용
df15=df15[['구인수','구직자수','취업자수']]
df17=df17[['구인수','구직자수','취업자수']]
```


```python
df13
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>구인수</th>
      <th>구직자수</th>
      <th>취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>20080</td>
      <td>10453</td>
      <td>6249</td>
    </tr>
  </tbody>
</table>
</div>




```python
df15
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>구인수</th>
      <th>구직자수</th>
      <th>취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>17748</td>
      <td>10923</td>
      <td>5962</td>
    </tr>
  </tbody>
</table>
</div>




```python
df17
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>구인수</th>
      <th>구직자수</th>
      <th>취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>19261</td>
      <td>14329</td>
      <td>5859</td>
    </tr>
  </tbody>
</table>
</div>




```python
df13=df13.T  # 행과 열 반전
df15=df15.T
df17=df17.T
```


```python
df13
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>구인수</th>
      <td>20080</td>
    </tr>
    <tr>
      <th>구직자수</th>
      <td>10453</td>
    </tr>
    <tr>
      <th>취업자수</th>
      <td>6249</td>
    </tr>
  </tbody>
</table>
</div>




```python
df15
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>구인수</th>
      <td>17748</td>
    </tr>
    <tr>
      <th>구직자수</th>
      <td>10923</td>
    </tr>
    <tr>
      <th>취업자수</th>
      <td>5962</td>
    </tr>
  </tbody>
</table>
</div>




```python
df17
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>구인수</th>
      <td>19261</td>
    </tr>
    <tr>
      <th>구직자수</th>
      <td>14329</td>
    </tr>
    <tr>
      <th>취업자수</th>
      <td>5859</td>
    </tr>
  </tbody>
</table>
</div>




```python
df13 = df13.rename(columns={0:'인원'})  # 컬럼명 변경
df15 = df15.rename(columns={0:'인원'})
df17 = df17.rename(columns={0:'인원'})
```


```python
df13
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>인원</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>구인수</th>
      <td>20080</td>
    </tr>
    <tr>
      <th>구직자수</th>
      <td>10453</td>
    </tr>
    <tr>
      <th>취업자수</th>
      <td>6249</td>
    </tr>
  </tbody>
</table>
</div>




```python
df15
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>인원</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>구인수</th>
      <td>17748</td>
    </tr>
    <tr>
      <th>구직자수</th>
      <td>10923</td>
    </tr>
    <tr>
      <th>취업자수</th>
      <td>5962</td>
    </tr>
  </tbody>
</table>
</div>




```python
df17
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>인원</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>구인수</th>
      <td>19261</td>
    </tr>
    <tr>
      <th>구직자수</th>
      <td>14329</td>
    </tr>
    <tr>
      <th>취업자수</th>
      <td>5859</td>
    </tr>
  </tbody>
</table>
</div>




```python
df13=df13.T  # 행과 열 반전
df15=df15.T
df17=df17.T
```


```python
df13
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>구인수</th>
      <th>구직자수</th>
      <th>취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>인원</th>
      <td>20080</td>
      <td>10453</td>
      <td>6249</td>
    </tr>
  </tbody>
</table>
</div>




```python
df15
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>구인수</th>
      <th>구직자수</th>
      <th>취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>인원</th>
      <td>17748</td>
      <td>10923</td>
      <td>5962</td>
    </tr>
  </tbody>
</table>
</div>




```python
df17
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>구인수</th>
      <th>구직자수</th>
      <th>취업자수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>인원</th>
      <td>19261</td>
      <td>14329</td>
      <td>5859</td>
    </tr>
  </tbody>
</table>
</div>




```python
from matplotlib.pyplot import figure
plt.style.use('ggplot')
df13.plot(kind='bar', title='장애인 일자리 현황(2013)',figsize=(8,5),ylim=(0,25000))
```




    <matplotlib.axes._subplots.AxesSubplot at 0x1a0558b8da0>




![png](output_49_1.png)



```python
from matplotlib.pyplot import figure
plt.style.use('ggplot')
df15.plot(kind='bar', title='장애인 일자리 현황(2015)',figsize=(8,5),ylim=(0,25000))
```




    <matplotlib.axes._subplots.AxesSubplot at 0x1a0558d6668>




![png](output_50_1.png)



```python
from matplotlib.pyplot import figure
plt.style.use('ggplot')
df17.plot(kind='bar', title='장애인 일자리 현황(2017)',figsize=(8,5),ylim=(0,25000))
```




    <matplotlib.axes._subplots.AxesSubplot at 0x1a054526b00>




![png](output_51_1.png)



```python
df2.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>미채용이유별(1)</th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
      <th>2017.1</th>
      <th>2017.2</th>
      <th>2017.3</th>
      <th>2017.4</th>
      <th>2017.5</th>
      <th>2017.6</th>
      <th>2017.7</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>미채용이유별(1)</td>
      <td>미채용이유별(2)</td>
      <td>전체(미고용)</td>
      <td>비의무</td>
      <td>비의무</td>
      <td>의무(50명 이상)</td>
      <td>의무(50명 이상)</td>
      <td>의무(50명 이상)</td>
      <td>의무(50명 이상)</td>
      <td>부담금 납부 대상(100명 이상)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>미채용이유별(1)</td>
      <td>미채용이유별(2)</td>
      <td>소계</td>
      <td>소계</td>
      <td>5~49명</td>
      <td>소계</td>
      <td>50~299명</td>
      <td>300~999명</td>
      <td>1,000명 이상</td>
      <td>소계</td>
    </tr>
    <tr>
      <th>2</th>
      <td>장애인 근로자를 고용하지 않은 이유</td>
      <td>장애인을 고용할 의사가 없어서</td>
      <td>83</td>
      <td>84.4</td>
      <td>84.4</td>
      <td>43.3</td>
      <td>43.4</td>
      <td>34</td>
      <td>66.7</td>
      <td>33.4</td>
    </tr>
    <tr>
      <th>3</th>
      <td>NaN</td>
      <td>장애인을 고용할 의사는 있으나 채용하지 못함</td>
      <td>17</td>
      <td>15.6</td>
      <td>15.6</td>
      <td>56.7</td>
      <td>56.6</td>
      <td>66</td>
      <td>33.3</td>
      <td>66.6</td>
    </tr>
    <tr>
      <th>4</th>
      <td>장애인 고용 의사가 없었던 주된 이유</td>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>57.5</td>
      <td>57.3</td>
      <td>57.3</td>
      <td>69.4</td>
      <td>69</td>
      <td>89.8</td>
      <td>100</td>
      <td>84.1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2=df2.head(4)   # 앞에서 4번행 까지만 사용
```


```python
df2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>미채용이유별(1)</th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
      <th>2017.1</th>
      <th>2017.2</th>
      <th>2017.3</th>
      <th>2017.4</th>
      <th>2017.5</th>
      <th>2017.6</th>
      <th>2017.7</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>미채용이유별(1)</td>
      <td>미채용이유별(2)</td>
      <td>전체(미고용)</td>
      <td>비의무</td>
      <td>비의무</td>
      <td>의무(50명 이상)</td>
      <td>의무(50명 이상)</td>
      <td>의무(50명 이상)</td>
      <td>의무(50명 이상)</td>
      <td>부담금 납부 대상(100명 이상)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>미채용이유별(1)</td>
      <td>미채용이유별(2)</td>
      <td>소계</td>
      <td>소계</td>
      <td>5~49명</td>
      <td>소계</td>
      <td>50~299명</td>
      <td>300~999명</td>
      <td>1,000명 이상</td>
      <td>소계</td>
    </tr>
    <tr>
      <th>2</th>
      <td>장애인 근로자를 고용하지 않은 이유</td>
      <td>장애인을 고용할 의사가 없어서</td>
      <td>83</td>
      <td>84.4</td>
      <td>84.4</td>
      <td>43.3</td>
      <td>43.4</td>
      <td>34</td>
      <td>66.7</td>
      <td>33.4</td>
    </tr>
    <tr>
      <th>3</th>
      <td>NaN</td>
      <td>장애인을 고용할 의사는 있으나 채용하지 못함</td>
      <td>17</td>
      <td>15.6</td>
      <td>15.6</td>
      <td>56.7</td>
      <td>56.6</td>
      <td>66</td>
      <td>33.3</td>
      <td>66.6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2=df2.drop(0).drop(1) # 사용하지 않을 0,1번 행을 삭제
```


```python
df2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>미채용이유별(1)</th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
      <th>2017.1</th>
      <th>2017.2</th>
      <th>2017.3</th>
      <th>2017.4</th>
      <th>2017.5</th>
      <th>2017.6</th>
      <th>2017.7</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2</th>
      <td>장애인 근로자를 고용하지 않은 이유</td>
      <td>장애인을 고용할 의사가 없어서</td>
      <td>83</td>
      <td>84.4</td>
      <td>84.4</td>
      <td>43.3</td>
      <td>43.4</td>
      <td>34</td>
      <td>66.7</td>
      <td>33.4</td>
    </tr>
    <tr>
      <th>3</th>
      <td>NaN</td>
      <td>장애인을 고용할 의사는 있으나 채용하지 못함</td>
      <td>17</td>
      <td>15.6</td>
      <td>15.6</td>
      <td>56.7</td>
      <td>56.6</td>
      <td>66</td>
      <td>33.3</td>
      <td>66.6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2=df2[['미채용이유별(2)','2017']]  # 사용할 정보만 추출
```


```python
df2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2</th>
      <td>장애인을 고용할 의사가 없어서</td>
      <td>83</td>
    </tr>
    <tr>
      <th>3</th>
      <td>장애인을 고용할 의사는 있으나 채용하지 못함</td>
      <td>17</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2=df2.reset_index()  # 인덱스 리셋
```


```python
df2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2</td>
      <td>장애인을 고용할 의사가 없어서</td>
      <td>83</td>
    </tr>
    <tr>
      <th>1</th>
      <td>3</td>
      <td>장애인을 고용할 의사는 있으나 채용하지 못함</td>
      <td>17</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2=df2.rename(columns={'미채용이유별(2)':'미채용 이유','2017':'비율'})   # 컬럼명 변경
```


```python
df2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>미채용 이유</th>
      <th>비율</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2</td>
      <td>장애인을 고용할 의사가 없어서</td>
      <td>83</td>
    </tr>
    <tr>
      <th>1</th>
      <td>3</td>
      <td>장애인을 고용할 의사는 있으나 채용하지 못함</td>
      <td>17</td>
    </tr>
  </tbody>
</table>
</div>




```python
import matplotlib.pyplot as plt
# pie 그래프
plt.rcParams['figure.figsize'] = [12, 8]  # 사이즈 설정
plt.pie(df2['비율'], 

        explode=(0.1,0), # 첫 번째 파이가 튀어나오게 설정

        labels=df2['미채용 이유'], 

        colors=['yellowgreen','lightskyblue'], 

        autopct='%1.2f%%', # second decimal place

        shadow=True, 

        startangle=90,  #어디에서 시작할지 정함

        textprops={'fontsize': 14}) # text font size

plt.axis('equal') #  equal length of X and Y axis

plt.title('장애인 미채용 이유', fontsize=20)

plt.show()
```


![png](output_63_0.png)



```python
df3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>미채용이유별(1)</th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
      <th>2017.1</th>
      <th>2017.2</th>
      <th>2017.3</th>
      <th>2017.4</th>
      <th>2017.5</th>
      <th>2017.6</th>
      <th>2017.7</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>미채용이유별(1)</td>
      <td>미채용이유별(2)</td>
      <td>전체(미고용)</td>
      <td>비의무</td>
      <td>비의무</td>
      <td>의무(50명 이상)</td>
      <td>의무(50명 이상)</td>
      <td>의무(50명 이상)</td>
      <td>의무(50명 이상)</td>
      <td>부담금 납부 대상(100명 이상)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>미채용이유별(1)</td>
      <td>미채용이유별(2)</td>
      <td>소계</td>
      <td>소계</td>
      <td>5~49명</td>
      <td>소계</td>
      <td>50~299명</td>
      <td>300~999명</td>
      <td>1,000명 이상</td>
      <td>소계</td>
    </tr>
    <tr>
      <th>2</th>
      <td>장애인 근로자를 고용하지 않은 이유</td>
      <td>장애인을 고용할 의사가 없어서</td>
      <td>83</td>
      <td>84.4</td>
      <td>84.4</td>
      <td>43.3</td>
      <td>43.4</td>
      <td>34</td>
      <td>66.7</td>
      <td>33.4</td>
    </tr>
    <tr>
      <th>3</th>
      <td>NaN</td>
      <td>장애인을 고용할 의사는 있으나 채용하지 못함</td>
      <td>17</td>
      <td>15.6</td>
      <td>15.6</td>
      <td>56.7</td>
      <td>56.6</td>
      <td>66</td>
      <td>33.3</td>
      <td>66.6</td>
    </tr>
    <tr>
      <th>4</th>
      <td>장애인 고용 의사가 없었던 주된 이유</td>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>57.5</td>
      <td>57.3</td>
      <td>57.3</td>
      <td>69.4</td>
      <td>69</td>
      <td>89.8</td>
      <td>100</td>
      <td>84.1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>NaN</td>
      <td>장애인 근로자의 생산성이 낮을 것 같아서</td>
      <td>8.5</td>
      <td>8.6</td>
      <td>8.6</td>
      <td>5.1</td>
      <td>5.2</td>
      <td>0</td>
      <td>0</td>
      <td>1.4</td>
    </tr>
    <tr>
      <th>6</th>
      <td>NaN</td>
      <td>과거 채용을 시도했으나 장애인 지원자 자체가 없어서</td>
      <td>1.2</td>
      <td>1.1</td>
      <td>1.1</td>
      <td>4.7</td>
      <td>4.8</td>
      <td>0</td>
      <td>0</td>
      <td>2.3</td>
    </tr>
    <tr>
      <th>7</th>
      <td>NaN</td>
      <td>장애인 고용에 따른 추가비용이 과다할 것 같아서</td>
      <td>1.3</td>
      <td>1.3</td>
      <td>1.3</td>
      <td>2.5</td>
      <td>2.6</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>8</th>
      <td>NaN</td>
      <td>사업주, 관리자, 동료 등이 장애인 채용을 꺼려서</td>
      <td>1.3</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>0.9</td>
      <td>0.9</td>
      <td>3.1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9</th>
      <td>NaN</td>
      <td>채용 후 인사관리가 어려울 것 같아서</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>2.1</td>
      <td>2.2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10</th>
      <td>NaN</td>
      <td>장애인용 시설 및 장비, 편의시설 등이 부족해서</td>
      <td>4</td>
      <td>4</td>
      <td>4</td>
      <td>4.3</td>
      <td>4.4</td>
      <td>0</td>
      <td>0</td>
      <td>1.9</td>
    </tr>
    <tr>
      <th>11</th>
      <td>NaN</td>
      <td>근무환경이 유해하거나 위험해서</td>
      <td>9.6</td>
      <td>9.6</td>
      <td>9.6</td>
      <td>7.2</td>
      <td>7.2</td>
      <td>7.1</td>
      <td>0</td>
      <td>7.3</td>
    </tr>
    <tr>
      <th>12</th>
      <td>NaN</td>
      <td>부담금 납부 대상이 아님에 따라(상시 100인 미만)</td>
      <td>15.1</td>
      <td>15.3</td>
      <td>15.3</td>
      <td>3.6</td>
      <td>3.7</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>13</th>
      <td>장애인 고용 의사는 있으나 채용하지 못했던 주된 이유</td>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>30.9</td>
      <td>29.6</td>
      <td>29.6</td>
      <td>41.5</td>
      <td>41.6</td>
      <td>38.9</td>
      <td>0</td>
      <td>38.7</td>
    </tr>
    <tr>
      <th>14</th>
      <td>NaN</td>
      <td>업무능력을 갖춘 장애인이 부족해서</td>
      <td>26.8</td>
      <td>27.3</td>
      <td>27.3</td>
      <td>23.2</td>
      <td>23.2</td>
      <td>23.8</td>
      <td>40</td>
      <td>20.6</td>
    </tr>
    <tr>
      <th>15</th>
      <td>NaN</td>
      <td>장애인 지원자 자체가 없어서</td>
      <td>20.9</td>
      <td>21</td>
      <td>21</td>
      <td>20</td>
      <td>20.1</td>
      <td>17.1</td>
      <td>0</td>
      <td>20.2</td>
    </tr>
    <tr>
      <th>16</th>
      <td>NaN</td>
      <td>인력채용 자체가 없어서</td>
      <td>16.4</td>
      <td>18</td>
      <td>18</td>
      <td>4.4</td>
      <td>4.2</td>
      <td>10.9</td>
      <td>60</td>
      <td>5.7</td>
    </tr>
    <tr>
      <th>17</th>
      <td>NaN</td>
      <td>장애인을 어떻게 채용할지 몰라서</td>
      <td>0.2</td>
      <td>0</td>
      <td>0</td>
      <td>1.8</td>
      <td>1.8</td>
      <td>2.1</td>
      <td>0</td>
      <td>1.7</td>
    </tr>
    <tr>
      <th>18</th>
      <td>NaN</td>
      <td>채용 후 인사관리가 어려울 것 같아서</td>
      <td>2.6</td>
      <td>2.7</td>
      <td>2.7</td>
      <td>1.2</td>
      <td>1.3</td>
      <td>0</td>
      <td>0</td>
      <td>0.9</td>
    </tr>
    <tr>
      <th>19</th>
      <td>NaN</td>
      <td>장애인용 시설 및 장비/편의시설 등이 부족해서</td>
      <td>0.3</td>
      <td>0</td>
      <td>0</td>
      <td>2.7</td>
      <td>2.7</td>
      <td>0</td>
      <td>0</td>
      <td>4.5</td>
    </tr>
    <tr>
      <th>20</th>
      <td>NaN</td>
      <td>근무환경이 유해하거나 위험해서</td>
      <td>1.2</td>
      <td>1.1</td>
      <td>1.1</td>
      <td>2.5</td>
      <td>2.5</td>
      <td>0</td>
      <td>0</td>
      <td>1.6</td>
    </tr>
    <tr>
      <th>21</th>
      <td>NaN</td>
      <td>일시적인 미고용 상태로 향후 즉시 채용 예정임</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.4</td>
      <td>2.6</td>
      <td>2.5</td>
      <td>7.3</td>
      <td>0</td>
      <td>6.1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df3=df3.drop(0).drop(1).drop(2).drop(3)  # 필요없는 행 삭제
```


```python
df3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>미채용이유별(1)</th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
      <th>2017.1</th>
      <th>2017.2</th>
      <th>2017.3</th>
      <th>2017.4</th>
      <th>2017.5</th>
      <th>2017.6</th>
      <th>2017.7</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>4</th>
      <td>장애인 고용 의사가 없었던 주된 이유</td>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>57.5</td>
      <td>57.3</td>
      <td>57.3</td>
      <td>69.4</td>
      <td>69</td>
      <td>89.8</td>
      <td>100</td>
      <td>84.1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>NaN</td>
      <td>장애인 근로자의 생산성이 낮을 것 같아서</td>
      <td>8.5</td>
      <td>8.6</td>
      <td>8.6</td>
      <td>5.1</td>
      <td>5.2</td>
      <td>0</td>
      <td>0</td>
      <td>1.4</td>
    </tr>
    <tr>
      <th>6</th>
      <td>NaN</td>
      <td>과거 채용을 시도했으나 장애인 지원자 자체가 없어서</td>
      <td>1.2</td>
      <td>1.1</td>
      <td>1.1</td>
      <td>4.7</td>
      <td>4.8</td>
      <td>0</td>
      <td>0</td>
      <td>2.3</td>
    </tr>
    <tr>
      <th>7</th>
      <td>NaN</td>
      <td>장애인 고용에 따른 추가비용이 과다할 것 같아서</td>
      <td>1.3</td>
      <td>1.3</td>
      <td>1.3</td>
      <td>2.5</td>
      <td>2.6</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>8</th>
      <td>NaN</td>
      <td>사업주, 관리자, 동료 등이 장애인 채용을 꺼려서</td>
      <td>1.3</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>0.9</td>
      <td>0.9</td>
      <td>3.1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9</th>
      <td>NaN</td>
      <td>채용 후 인사관리가 어려울 것 같아서</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>2.1</td>
      <td>2.2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>10</th>
      <td>NaN</td>
      <td>장애인용 시설 및 장비, 편의시설 등이 부족해서</td>
      <td>4</td>
      <td>4</td>
      <td>4</td>
      <td>4.3</td>
      <td>4.4</td>
      <td>0</td>
      <td>0</td>
      <td>1.9</td>
    </tr>
    <tr>
      <th>11</th>
      <td>NaN</td>
      <td>근무환경이 유해하거나 위험해서</td>
      <td>9.6</td>
      <td>9.6</td>
      <td>9.6</td>
      <td>7.2</td>
      <td>7.2</td>
      <td>7.1</td>
      <td>0</td>
      <td>7.3</td>
    </tr>
    <tr>
      <th>12</th>
      <td>NaN</td>
      <td>부담금 납부 대상이 아님에 따라(상시 100인 미만)</td>
      <td>15.1</td>
      <td>15.3</td>
      <td>15.3</td>
      <td>3.6</td>
      <td>3.7</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>13</th>
      <td>장애인 고용 의사는 있으나 채용하지 못했던 주된 이유</td>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>30.9</td>
      <td>29.6</td>
      <td>29.6</td>
      <td>41.5</td>
      <td>41.6</td>
      <td>38.9</td>
      <td>0</td>
      <td>38.7</td>
    </tr>
    <tr>
      <th>14</th>
      <td>NaN</td>
      <td>업무능력을 갖춘 장애인이 부족해서</td>
      <td>26.8</td>
      <td>27.3</td>
      <td>27.3</td>
      <td>23.2</td>
      <td>23.2</td>
      <td>23.8</td>
      <td>40</td>
      <td>20.6</td>
    </tr>
    <tr>
      <th>15</th>
      <td>NaN</td>
      <td>장애인 지원자 자체가 없어서</td>
      <td>20.9</td>
      <td>21</td>
      <td>21</td>
      <td>20</td>
      <td>20.1</td>
      <td>17.1</td>
      <td>0</td>
      <td>20.2</td>
    </tr>
    <tr>
      <th>16</th>
      <td>NaN</td>
      <td>인력채용 자체가 없어서</td>
      <td>16.4</td>
      <td>18</td>
      <td>18</td>
      <td>4.4</td>
      <td>4.2</td>
      <td>10.9</td>
      <td>60</td>
      <td>5.7</td>
    </tr>
    <tr>
      <th>17</th>
      <td>NaN</td>
      <td>장애인을 어떻게 채용할지 몰라서</td>
      <td>0.2</td>
      <td>0</td>
      <td>0</td>
      <td>1.8</td>
      <td>1.8</td>
      <td>2.1</td>
      <td>0</td>
      <td>1.7</td>
    </tr>
    <tr>
      <th>18</th>
      <td>NaN</td>
      <td>채용 후 인사관리가 어려울 것 같아서</td>
      <td>2.6</td>
      <td>2.7</td>
      <td>2.7</td>
      <td>1.2</td>
      <td>1.3</td>
      <td>0</td>
      <td>0</td>
      <td>0.9</td>
    </tr>
    <tr>
      <th>19</th>
      <td>NaN</td>
      <td>장애인용 시설 및 장비/편의시설 등이 부족해서</td>
      <td>0.3</td>
      <td>0</td>
      <td>0</td>
      <td>2.7</td>
      <td>2.7</td>
      <td>0</td>
      <td>0</td>
      <td>4.5</td>
    </tr>
    <tr>
      <th>20</th>
      <td>NaN</td>
      <td>근무환경이 유해하거나 위험해서</td>
      <td>1.2</td>
      <td>1.1</td>
      <td>1.1</td>
      <td>2.5</td>
      <td>2.5</td>
      <td>0</td>
      <td>0</td>
      <td>1.6</td>
    </tr>
    <tr>
      <th>21</th>
      <td>NaN</td>
      <td>일시적인 미고용 상태로 향후 즉시 채용 예정임</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.4</td>
      <td>2.6</td>
      <td>2.5</td>
      <td>7.3</td>
      <td>0</td>
      <td>6.1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df3=df3.reset_index() # 인덱스 리셋
```


```python
df3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>미채용이유별(1)</th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
      <th>2017.1</th>
      <th>2017.2</th>
      <th>2017.3</th>
      <th>2017.4</th>
      <th>2017.5</th>
      <th>2017.6</th>
      <th>2017.7</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>4</td>
      <td>장애인 고용 의사가 없었던 주된 이유</td>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>57.5</td>
      <td>57.3</td>
      <td>57.3</td>
      <td>69.4</td>
      <td>69</td>
      <td>89.8</td>
      <td>100</td>
      <td>84.1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5</td>
      <td>NaN</td>
      <td>장애인 근로자의 생산성이 낮을 것 같아서</td>
      <td>8.5</td>
      <td>8.6</td>
      <td>8.6</td>
      <td>5.1</td>
      <td>5.2</td>
      <td>0</td>
      <td>0</td>
      <td>1.4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>6</td>
      <td>NaN</td>
      <td>과거 채용을 시도했으나 장애인 지원자 자체가 없어서</td>
      <td>1.2</td>
      <td>1.1</td>
      <td>1.1</td>
      <td>4.7</td>
      <td>4.8</td>
      <td>0</td>
      <td>0</td>
      <td>2.3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>7</td>
      <td>NaN</td>
      <td>장애인 고용에 따른 추가비용이 과다할 것 같아서</td>
      <td>1.3</td>
      <td>1.3</td>
      <td>1.3</td>
      <td>2.5</td>
      <td>2.6</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>NaN</td>
      <td>사업주, 관리자, 동료 등이 장애인 채용을 꺼려서</td>
      <td>1.3</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>0.9</td>
      <td>0.9</td>
      <td>3.1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>9</td>
      <td>NaN</td>
      <td>채용 후 인사관리가 어려울 것 같아서</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>2.1</td>
      <td>2.2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>10</td>
      <td>NaN</td>
      <td>장애인용 시설 및 장비, 편의시설 등이 부족해서</td>
      <td>4</td>
      <td>4</td>
      <td>4</td>
      <td>4.3</td>
      <td>4.4</td>
      <td>0</td>
      <td>0</td>
      <td>1.9</td>
    </tr>
    <tr>
      <th>7</th>
      <td>11</td>
      <td>NaN</td>
      <td>근무환경이 유해하거나 위험해서</td>
      <td>9.6</td>
      <td>9.6</td>
      <td>9.6</td>
      <td>7.2</td>
      <td>7.2</td>
      <td>7.1</td>
      <td>0</td>
      <td>7.3</td>
    </tr>
    <tr>
      <th>8</th>
      <td>12</td>
      <td>NaN</td>
      <td>부담금 납부 대상이 아님에 따라(상시 100인 미만)</td>
      <td>15.1</td>
      <td>15.3</td>
      <td>15.3</td>
      <td>3.6</td>
      <td>3.7</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>9</th>
      <td>13</td>
      <td>장애인 고용 의사는 있으나 채용하지 못했던 주된 이유</td>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>30.9</td>
      <td>29.6</td>
      <td>29.6</td>
      <td>41.5</td>
      <td>41.6</td>
      <td>38.9</td>
      <td>0</td>
      <td>38.7</td>
    </tr>
    <tr>
      <th>10</th>
      <td>14</td>
      <td>NaN</td>
      <td>업무능력을 갖춘 장애인이 부족해서</td>
      <td>26.8</td>
      <td>27.3</td>
      <td>27.3</td>
      <td>23.2</td>
      <td>23.2</td>
      <td>23.8</td>
      <td>40</td>
      <td>20.6</td>
    </tr>
    <tr>
      <th>11</th>
      <td>15</td>
      <td>NaN</td>
      <td>장애인 지원자 자체가 없어서</td>
      <td>20.9</td>
      <td>21</td>
      <td>21</td>
      <td>20</td>
      <td>20.1</td>
      <td>17.1</td>
      <td>0</td>
      <td>20.2</td>
    </tr>
    <tr>
      <th>12</th>
      <td>16</td>
      <td>NaN</td>
      <td>인력채용 자체가 없어서</td>
      <td>16.4</td>
      <td>18</td>
      <td>18</td>
      <td>4.4</td>
      <td>4.2</td>
      <td>10.9</td>
      <td>60</td>
      <td>5.7</td>
    </tr>
    <tr>
      <th>13</th>
      <td>17</td>
      <td>NaN</td>
      <td>장애인을 어떻게 채용할지 몰라서</td>
      <td>0.2</td>
      <td>0</td>
      <td>0</td>
      <td>1.8</td>
      <td>1.8</td>
      <td>2.1</td>
      <td>0</td>
      <td>1.7</td>
    </tr>
    <tr>
      <th>14</th>
      <td>18</td>
      <td>NaN</td>
      <td>채용 후 인사관리가 어려울 것 같아서</td>
      <td>2.6</td>
      <td>2.7</td>
      <td>2.7</td>
      <td>1.2</td>
      <td>1.3</td>
      <td>0</td>
      <td>0</td>
      <td>0.9</td>
    </tr>
    <tr>
      <th>15</th>
      <td>19</td>
      <td>NaN</td>
      <td>장애인용 시설 및 장비/편의시설 등이 부족해서</td>
      <td>0.3</td>
      <td>0</td>
      <td>0</td>
      <td>2.7</td>
      <td>2.7</td>
      <td>0</td>
      <td>0</td>
      <td>4.5</td>
    </tr>
    <tr>
      <th>16</th>
      <td>20</td>
      <td>NaN</td>
      <td>근무환경이 유해하거나 위험해서</td>
      <td>1.2</td>
      <td>1.1</td>
      <td>1.1</td>
      <td>2.5</td>
      <td>2.5</td>
      <td>0</td>
      <td>0</td>
      <td>1.6</td>
    </tr>
    <tr>
      <th>17</th>
      <td>21</td>
      <td>NaN</td>
      <td>일시적인 미고용 상태로 향후 즉시 채용 예정임</td>
      <td>0.6</td>
      <td>0.4</td>
      <td>0.4</td>
      <td>2.6</td>
      <td>2.5</td>
      <td>7.3</td>
      <td>0</td>
      <td>6.1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df3=df3.head(9) # 9번째 행까지만 사용
```


```python
df3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>미채용이유별(1)</th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
      <th>2017.1</th>
      <th>2017.2</th>
      <th>2017.3</th>
      <th>2017.4</th>
      <th>2017.5</th>
      <th>2017.6</th>
      <th>2017.7</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>4</td>
      <td>장애인 고용 의사가 없었던 주된 이유</td>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>57.5</td>
      <td>57.3</td>
      <td>57.3</td>
      <td>69.4</td>
      <td>69</td>
      <td>89.8</td>
      <td>100</td>
      <td>84.1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5</td>
      <td>NaN</td>
      <td>장애인 근로자의 생산성이 낮을 것 같아서</td>
      <td>8.5</td>
      <td>8.6</td>
      <td>8.6</td>
      <td>5.1</td>
      <td>5.2</td>
      <td>0</td>
      <td>0</td>
      <td>1.4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>6</td>
      <td>NaN</td>
      <td>과거 채용을 시도했으나 장애인 지원자 자체가 없어서</td>
      <td>1.2</td>
      <td>1.1</td>
      <td>1.1</td>
      <td>4.7</td>
      <td>4.8</td>
      <td>0</td>
      <td>0</td>
      <td>2.3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>7</td>
      <td>NaN</td>
      <td>장애인 고용에 따른 추가비용이 과다할 것 같아서</td>
      <td>1.3</td>
      <td>1.3</td>
      <td>1.3</td>
      <td>2.5</td>
      <td>2.6</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>NaN</td>
      <td>사업주, 관리자, 동료 등이 장애인 채용을 꺼려서</td>
      <td>1.3</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>0.9</td>
      <td>0.9</td>
      <td>3.1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>9</td>
      <td>NaN</td>
      <td>채용 후 인사관리가 어려울 것 같아서</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>1.4</td>
      <td>2.1</td>
      <td>2.2</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>10</td>
      <td>NaN</td>
      <td>장애인용 시설 및 장비, 편의시설 등이 부족해서</td>
      <td>4</td>
      <td>4</td>
      <td>4</td>
      <td>4.3</td>
      <td>4.4</td>
      <td>0</td>
      <td>0</td>
      <td>1.9</td>
    </tr>
    <tr>
      <th>7</th>
      <td>11</td>
      <td>NaN</td>
      <td>근무환경이 유해하거나 위험해서</td>
      <td>9.6</td>
      <td>9.6</td>
      <td>9.6</td>
      <td>7.2</td>
      <td>7.2</td>
      <td>7.1</td>
      <td>0</td>
      <td>7.3</td>
    </tr>
    <tr>
      <th>8</th>
      <td>12</td>
      <td>NaN</td>
      <td>부담금 납부 대상이 아님에 따라(상시 100인 미만)</td>
      <td>15.1</td>
      <td>15.3</td>
      <td>15.3</td>
      <td>3.6</td>
      <td>3.7</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
df3=df3[['미채용이유별(2)','2017']]  # 필요한 정보 추출
```


```python
df3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>57.5</td>
    </tr>
    <tr>
      <th>1</th>
      <td>장애인 근로자의 생산성이 낮을 것 같아서</td>
      <td>8.5</td>
    </tr>
    <tr>
      <th>2</th>
      <td>과거 채용을 시도했으나 장애인 지원자 자체가 없어서</td>
      <td>1.2</td>
    </tr>
    <tr>
      <th>3</th>
      <td>장애인 고용에 따른 추가비용이 과다할 것 같아서</td>
      <td>1.3</td>
    </tr>
    <tr>
      <th>4</th>
      <td>사업주, 관리자, 동료 등이 장애인 채용을 꺼려서</td>
      <td>1.3</td>
    </tr>
    <tr>
      <th>5</th>
      <td>채용 후 인사관리가 어려울 것 같아서</td>
      <td>1.4</td>
    </tr>
    <tr>
      <th>6</th>
      <td>장애인용 시설 및 장비, 편의시설 등이 부족해서</td>
      <td>4</td>
    </tr>
    <tr>
      <th>7</th>
      <td>근무환경이 유해하거나 위험해서</td>
      <td>9.6</td>
    </tr>
    <tr>
      <th>8</th>
      <td>부담금 납부 대상이 아님에 따라(상시 100인 미만)</td>
      <td>15.1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df3=df3.replace(['과거 채용을 시도했으나 장애인 지원자 자체가 없어서','장애인 고용에 따른 추가비용이 과다할 것 같아서',
            '사업주, 관리자, 동료 등이 장애인 채용을 꺼려서','채용 후 인사관리가 어려울 것 같아서',
            '장애인용 시설 및 장비, 편의시설 등이 부족해서'],['기타','기타','기타','기타','기타'])  # 5이하의 값들의 이름을 기타로 변경
```


```python
df3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>57.5</td>
    </tr>
    <tr>
      <th>1</th>
      <td>장애인 근로자의 생산성이 낮을 것 같아서</td>
      <td>8.5</td>
    </tr>
    <tr>
      <th>2</th>
      <td>기타</td>
      <td>1.2</td>
    </tr>
    <tr>
      <th>3</th>
      <td>기타</td>
      <td>1.3</td>
    </tr>
    <tr>
      <th>4</th>
      <td>기타</td>
      <td>1.3</td>
    </tr>
    <tr>
      <th>5</th>
      <td>기타</td>
      <td>1.4</td>
    </tr>
    <tr>
      <th>6</th>
      <td>기타</td>
      <td>4.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>근무환경이 유해하거나 위험해서</td>
      <td>9.6</td>
    </tr>
    <tr>
      <th>8</th>
      <td>부담금 납부 대상이 아님에 따라(상시 100인 미만)</td>
      <td>15.1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df3=df3.groupby('미채용이유별(2)')['2017'].sum()  # 미채용이유별(2), 2017의 합 그룹핑
```


```python
df3
```




    미채용이유별(2)
    근무환경이 유해하거나 위험해서                  9.6
    기타                                9.2
    부담금 납부 대상이 아님에 따라(상시 100인 미만)    15.1
    장애인 근로자의 생산성이 낮을 것 같아서            8.5
    장애인에게 적합한 직무가 부족하거나 찾지 못해서       57.5
    Name: 2017, dtype: float64




```python
df3=df3.sort_values(ascending=False) # 큰것부터 정렬
```


```python
df3
```




    미채용이유별(2)
    장애인에게 적합한 직무가 부족하거나 찾지 못해서       57.5
    부담금 납부 대상이 아님에 따라(상시 100인 미만)    15.1
    근무환경이 유해하거나 위험해서                  9.6
    기타                                9.2
    장애인 근로자의 생산성이 낮을 것 같아서            8.5
    Name: 2017, dtype: float64




```python
df3=pd.DataFrame(df3)   # 데이터프레임 생성
```


```python
df3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>2017</th>
    </tr>
    <tr>
      <th>미채용이유별(2)</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>장애인에게 적합한 직무가 부족하거나 찾지 못해서</th>
      <td>57.5</td>
    </tr>
    <tr>
      <th>부담금 납부 대상이 아님에 따라(상시 100인 미만)</th>
      <td>15.1</td>
    </tr>
    <tr>
      <th>근무환경이 유해하거나 위험해서</th>
      <td>9.6</td>
    </tr>
    <tr>
      <th>기타</th>
      <td>9.2</td>
    </tr>
    <tr>
      <th>장애인 근로자의 생산성이 낮을 것 같아서</th>
      <td>8.5</td>
    </tr>
  </tbody>
</table>
</div>




```python
df3=df3.reset_index()  # 인덱스 리셋
```


```python
df3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>미채용이유별(2)</th>
      <th>2017</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>장애인에게 적합한 직무가 부족하거나 찾지 못해서</td>
      <td>57.5</td>
    </tr>
    <tr>
      <th>1</th>
      <td>부담금 납부 대상이 아님에 따라(상시 100인 미만)</td>
      <td>15.1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>근무환경이 유해하거나 위험해서</td>
      <td>9.6</td>
    </tr>
    <tr>
      <th>3</th>
      <td>기타</td>
      <td>9.2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>장애인 근로자의 생산성이 낮을 것 같아서</td>
      <td>8.5</td>
    </tr>
  </tbody>
</table>
</div>




```python
import matplotlib.pyplot as plt
# pie 그래프
plt.rcParams['figure.figsize'] = [12, 8]
plt.pie(df3['2017'], 

        explode=(0.1,0,0,0,0), # 첫 번째 파이만 튀어나오게 설정

        labels=df3['미채용이유별(2)'], 

        autopct='%1.2f%%', # second decimal place

        shadow=True, 

        startangle=90,   #어디에서 시작할지 정함

        textprops={'fontsize': 14}) # text font size

plt.axis('equal') #  equal length of X and Y axis

plt.title('고용할 의사가 없는 이유', fontsize=20)

plt.show()
```


![png](output_83_0.png)

