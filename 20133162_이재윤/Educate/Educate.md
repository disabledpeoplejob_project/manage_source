

```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
```


```python
from matplotlib import font_manager, rc
import matplotlib
font_location="c:/windows/fonts/malgun.ttf"
font_name=font_manager.FontProperties(fname=font_location).get_name()
matplotlib.rc('font',family=font_name)
```


```python
df1=pd.read_excel('민간직업교육.xlsx')  #파일 불러오기
```


```python
df2=pd.read_excel('공공직업교육.xlsx', header=2, usecols='A,C')
df4=pd.read_excel('교육경험.xlsx')
```


```python
df1.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>연 번</th>
      <th>지사명</th>
      <th>훈련기관</th>
      <th>훈련과정</th>
      <th>소재지</th>
      <th>대분류</th>
      <th>중분류</th>
      <th>소분류</th>
      <th>훈련비
기준단가</th>
      <th>훈련
개시일</th>
      <th>훈련
종료일</th>
      <th>개월수</th>
      <th>총
훈련시간</th>
      <th>1일 훈련시간</th>
      <th>선정인원</th>
      <th>선정결과</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>서울</td>
      <td>국제인재능력개발원</td>
      <td>재무∙회계</td>
      <td>서울특별시 동대문구</td>
      <td>02.경영·회계·사무</td>
      <td>03.재무·회계</td>
      <td>02.회계</td>
      <td>6419</td>
      <td>2019-03-04</td>
      <td>2019-10-28</td>
      <td>7.933333</td>
      <td>909</td>
      <td>6</td>
      <td>15</td>
      <td>선정</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>서울</td>
      <td>글로벌푸드아트수도직업전문학교</td>
      <td>한식∙양식조리 및 브런치카페 취업창업과정</td>
      <td>서울특별시 종로구</td>
      <td>13.음식서비스</td>
      <td>01.식음료조리·서비스</td>
      <td>01.음식조리</td>
      <td>7567</td>
      <td>2019-03-07</td>
      <td>2019-11-29</td>
      <td>8.900000</td>
      <td>1053</td>
      <td>6</td>
      <td>15</td>
      <td>선정</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>서울남부</td>
      <td>경영기술개발원</td>
      <td>반응형UI/UX전문가</td>
      <td>서울특별시 구로구</td>
      <td>20.정보통신</td>
      <td>01.정보기술</td>
      <td>02.정보기술개발</td>
      <td>6574</td>
      <td>2019-01-11</td>
      <td>2019-11-29</td>
      <td>10.733333</td>
      <td>1308</td>
      <td>6</td>
      <td>10</td>
      <td>선정</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>서울남부</td>
      <td>한국제과직업전문학교</td>
      <td>제과제빵</td>
      <td>서울특별시 영등포구</td>
      <td>21.식품가공</td>
      <td>02.제과∙제빵</td>
      <td>01.제과∙제빵</td>
      <td>6609</td>
      <td>2019-01-28</td>
      <td>2019-11-29</td>
      <td>10.166667</td>
      <td>1212</td>
      <td>6</td>
      <td>20</td>
      <td>선정</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>서울동부</td>
      <td>대신기술능력개발원</td>
      <td>디지털디자인∙사무전문가 양성과정</td>
      <td>서울특별시 강동구</td>
      <td>08.문화예술·디자인·방송</td>
      <td>02.디자인</td>
      <td>01.디자인</td>
      <td>5263</td>
      <td>2019-02-11</td>
      <td>2019-11-28</td>
      <td>9.666667</td>
      <td>1146</td>
      <td>6</td>
      <td>15</td>
      <td>선정</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2   #NaN 값은 지역을 소분류 해놓은 값이라 필요없음
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>소재지</th>
      <th>시설수 (개)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>전국</td>
      <td>27</td>
    </tr>
    <tr>
      <th>1</th>
      <td>부산</td>
      <td>5</td>
    </tr>
    <tr>
      <th>2</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>NaN</td>
      <td>2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>NaN</td>
      <td>2</td>
    </tr>
    <tr>
      <th>5</th>
      <td>대구</td>
      <td>1</td>
    </tr>
    <tr>
      <th>6</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>7</th>
      <td>광주</td>
      <td>1</td>
    </tr>
    <tr>
      <th>8</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9</th>
      <td>울산</td>
      <td>1</td>
    </tr>
    <tr>
      <th>10</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>11</th>
      <td>경기</td>
      <td>3</td>
    </tr>
    <tr>
      <th>12</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>13</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>14</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>15</th>
      <td>강원</td>
      <td>1</td>
    </tr>
    <tr>
      <th>16</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>17</th>
      <td>충북</td>
      <td>4</td>
    </tr>
    <tr>
      <th>18</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>19</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>20</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>21</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>22</th>
      <td>충남</td>
      <td>1</td>
    </tr>
    <tr>
      <th>23</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>24</th>
      <td>전북</td>
      <td>3</td>
    </tr>
    <tr>
      <th>25</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>26</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>27</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>28</th>
      <td>전남</td>
      <td>4</td>
    </tr>
    <tr>
      <th>29</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>30</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>31</th>
      <td>NaN</td>
      <td>2</td>
    </tr>
    <tr>
      <th>32</th>
      <td>경북</td>
      <td>2</td>
    </tr>
    <tr>
      <th>33</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>34</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
    <tr>
      <th>35</th>
      <td>경상</td>
      <td>1</td>
    </tr>
    <tr>
      <th>36</th>
      <td>NaN</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2.columns   # 컬럼명 확인
```




    Index(['소재지', '시설수 (개)'], dtype='object')




```python
df2.rename(columns={'소재지':'지사명'}, inplace=True)  # 컬럼명 변경
```


```python
df2=df2.drop([0])  #0번행 삭제
```


```python
su2=df2.groupby('지사명')['시설수 (개)'].sum()  # 지역별, 시설수 합 그룹핑
```


```python
su2
```




    지사명
    강원    1
    경기    3
    경북    2
    경상    1
    광주    1
    대구    1
    부산    5
    울산    1
    전남    4
    전북    3
    충남    1
    충북    4
    Name: 시설수 (개), dtype: int64




```python
su2=pd.DataFrame(su2)  # su2 데이터프레임 생성
```


```python
su2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>시설수 (개)</th>
    </tr>
    <tr>
      <th>지사명</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>강원</th>
      <td>1</td>
    </tr>
    <tr>
      <th>경기</th>
      <td>3</td>
    </tr>
    <tr>
      <th>경북</th>
      <td>2</td>
    </tr>
    <tr>
      <th>경상</th>
      <td>1</td>
    </tr>
    <tr>
      <th>광주</th>
      <td>1</td>
    </tr>
    <tr>
      <th>대구</th>
      <td>1</td>
    </tr>
    <tr>
      <th>부산</th>
      <td>5</td>
    </tr>
    <tr>
      <th>울산</th>
      <td>1</td>
    </tr>
    <tr>
      <th>전남</th>
      <td>4</td>
    </tr>
    <tr>
      <th>전북</th>
      <td>3</td>
    </tr>
    <tr>
      <th>충남</th>
      <td>1</td>
    </tr>
    <tr>
      <th>충북</th>
      <td>4</td>
    </tr>
  </tbody>
</table>
</div>




```python
su2=su2.rename(columns={'시설수 (개)':'시설수'})  # 컬럼명 변경
```


```python
su2=su2.reset_index()  # index를 리셋해 index부여
```


```python
su2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>지사명</th>
      <th>시설수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>강원</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>경기</td>
      <td>3</td>
    </tr>
    <tr>
      <th>2</th>
      <td>경북</td>
      <td>2</td>
    </tr>
    <tr>
      <th>3</th>
      <td>경상</td>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>광주</td>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>대구</td>
      <td>1</td>
    </tr>
    <tr>
      <th>6</th>
      <td>부산</td>
      <td>5</td>
    </tr>
    <tr>
      <th>7</th>
      <td>울산</td>
      <td>1</td>
    </tr>
    <tr>
      <th>8</th>
      <td>전남</td>
      <td>4</td>
    </tr>
    <tr>
      <th>9</th>
      <td>전북</td>
      <td>3</td>
    </tr>
    <tr>
      <th>10</th>
      <td>충남</td>
      <td>1</td>
    </tr>
    <tr>
      <th>11</th>
      <td>충북</td>
      <td>4</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1.columns  # df1의 필드명 확인
```




    Index(['연 번', '지사명', '훈련기관', '훈련과정', '소재지', '대분류', '중분류', '소분류', '훈련비\n기준단가',
           '훈련\n개시일', '훈련\n종료일', '개월수', '총\n훈련시간', '1일 훈련시간', '선정인원\n', '선정결과'],
          dtype='object')




```python
df1.groupby('지사명')['선정인원\n'].sum()  # 지역별 선정인원
```




    지사명
    강원       30
    경기       90
    경기북부     10
    경남       50
    경북       35
    광주       45
    대구       78
    대전       34
    부산      194
    서울      140
    서울남부     90
    서울동부    141
    울산       14
    인천       65
    전남       80
    전북       30
    제주       25
    충남       72
    충북       34
    Name: 선정인원\n, dtype: int64




```python
su=df1['지사명'].value_counts()  # 같은 지사명 개수 = 시설수
```


```python
su
```




    서울      12
    서울남부    12
    부산      12
    서울동부     7
    경기       6
    대구       6
    전남       5
    인천       5
    충남       4
    광주       4
    경남       4
    경북       4
    대전       3
    제주       3
    강원       2
    충북       2
    전북       2
    울산       2
    경기북부     1
    Name: 지사명, dtype: int64




```python
su1=pd.DataFrame(su)  # su 데이터프레임 생성 후 su1에 저장
```


```python
su1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>지사명</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>서울</th>
      <td>12</td>
    </tr>
    <tr>
      <th>서울남부</th>
      <td>12</td>
    </tr>
    <tr>
      <th>부산</th>
      <td>12</td>
    </tr>
    <tr>
      <th>서울동부</th>
      <td>7</td>
    </tr>
    <tr>
      <th>경기</th>
      <td>6</td>
    </tr>
    <tr>
      <th>대구</th>
      <td>6</td>
    </tr>
    <tr>
      <th>전남</th>
      <td>5</td>
    </tr>
    <tr>
      <th>인천</th>
      <td>5</td>
    </tr>
    <tr>
      <th>충남</th>
      <td>4</td>
    </tr>
    <tr>
      <th>광주</th>
      <td>4</td>
    </tr>
    <tr>
      <th>경남</th>
      <td>4</td>
    </tr>
    <tr>
      <th>경북</th>
      <td>4</td>
    </tr>
    <tr>
      <th>대전</th>
      <td>3</td>
    </tr>
    <tr>
      <th>제주</th>
      <td>3</td>
    </tr>
    <tr>
      <th>강원</th>
      <td>2</td>
    </tr>
    <tr>
      <th>충북</th>
      <td>2</td>
    </tr>
    <tr>
      <th>전북</th>
      <td>2</td>
    </tr>
    <tr>
      <th>울산</th>
      <td>2</td>
    </tr>
    <tr>
      <th>경기북부</th>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
su1=su1.reset_index() #index 리셋해 index 부여
```


```python
su1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>index</th>
      <th>지사명</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>서울</td>
      <td>12</td>
    </tr>
    <tr>
      <th>1</th>
      <td>서울남부</td>
      <td>12</td>
    </tr>
    <tr>
      <th>2</th>
      <td>부산</td>
      <td>12</td>
    </tr>
    <tr>
      <th>3</th>
      <td>서울동부</td>
      <td>7</td>
    </tr>
    <tr>
      <th>4</th>
      <td>경기</td>
      <td>6</td>
    </tr>
    <tr>
      <th>5</th>
      <td>대구</td>
      <td>6</td>
    </tr>
    <tr>
      <th>6</th>
      <td>전남</td>
      <td>5</td>
    </tr>
    <tr>
      <th>7</th>
      <td>인천</td>
      <td>5</td>
    </tr>
    <tr>
      <th>8</th>
      <td>충남</td>
      <td>4</td>
    </tr>
    <tr>
      <th>9</th>
      <td>광주</td>
      <td>4</td>
    </tr>
    <tr>
      <th>10</th>
      <td>경남</td>
      <td>4</td>
    </tr>
    <tr>
      <th>11</th>
      <td>경북</td>
      <td>4</td>
    </tr>
    <tr>
      <th>12</th>
      <td>대전</td>
      <td>3</td>
    </tr>
    <tr>
      <th>13</th>
      <td>제주</td>
      <td>3</td>
    </tr>
    <tr>
      <th>14</th>
      <td>강원</td>
      <td>2</td>
    </tr>
    <tr>
      <th>15</th>
      <td>충북</td>
      <td>2</td>
    </tr>
    <tr>
      <th>16</th>
      <td>전북</td>
      <td>2</td>
    </tr>
    <tr>
      <th>17</th>
      <td>울산</td>
      <td>2</td>
    </tr>
    <tr>
      <th>18</th>
      <td>경기북부</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
su1=su1.rename(columns={'지사명':'시설수'})  #
```


```python
su1=su1.rename(columns={'index':'지사명'})  # 필드명 변경
```


```python
su1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>지사명</th>
      <th>시설수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>서울</td>
      <td>12</td>
    </tr>
    <tr>
      <th>1</th>
      <td>서울남부</td>
      <td>12</td>
    </tr>
    <tr>
      <th>2</th>
      <td>부산</td>
      <td>12</td>
    </tr>
    <tr>
      <th>3</th>
      <td>서울동부</td>
      <td>7</td>
    </tr>
    <tr>
      <th>4</th>
      <td>경기</td>
      <td>6</td>
    </tr>
    <tr>
      <th>5</th>
      <td>대구</td>
      <td>6</td>
    </tr>
    <tr>
      <th>6</th>
      <td>전남</td>
      <td>5</td>
    </tr>
    <tr>
      <th>7</th>
      <td>인천</td>
      <td>5</td>
    </tr>
    <tr>
      <th>8</th>
      <td>충남</td>
      <td>4</td>
    </tr>
    <tr>
      <th>9</th>
      <td>광주</td>
      <td>4</td>
    </tr>
    <tr>
      <th>10</th>
      <td>경남</td>
      <td>4</td>
    </tr>
    <tr>
      <th>11</th>
      <td>경북</td>
      <td>4</td>
    </tr>
    <tr>
      <th>12</th>
      <td>대전</td>
      <td>3</td>
    </tr>
    <tr>
      <th>13</th>
      <td>제주</td>
      <td>3</td>
    </tr>
    <tr>
      <th>14</th>
      <td>강원</td>
      <td>2</td>
    </tr>
    <tr>
      <th>15</th>
      <td>충북</td>
      <td>2</td>
    </tr>
    <tr>
      <th>16</th>
      <td>전북</td>
      <td>2</td>
    </tr>
    <tr>
      <th>17</th>
      <td>울산</td>
      <td>2</td>
    </tr>
    <tr>
      <th>18</th>
      <td>경기북부</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
df3=pd.merge(su1,su2, how='outer')  # df3에 su1,su2를 합침
```


```python
df3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>지사명</th>
      <th>시설수</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>서울</td>
      <td>12</td>
    </tr>
    <tr>
      <th>1</th>
      <td>서울남부</td>
      <td>12</td>
    </tr>
    <tr>
      <th>2</th>
      <td>부산</td>
      <td>12</td>
    </tr>
    <tr>
      <th>3</th>
      <td>서울동부</td>
      <td>7</td>
    </tr>
    <tr>
      <th>4</th>
      <td>경기</td>
      <td>6</td>
    </tr>
    <tr>
      <th>5</th>
      <td>대구</td>
      <td>6</td>
    </tr>
    <tr>
      <th>6</th>
      <td>전남</td>
      <td>5</td>
    </tr>
    <tr>
      <th>7</th>
      <td>인천</td>
      <td>5</td>
    </tr>
    <tr>
      <th>8</th>
      <td>충남</td>
      <td>4</td>
    </tr>
    <tr>
      <th>9</th>
      <td>광주</td>
      <td>4</td>
    </tr>
    <tr>
      <th>10</th>
      <td>경남</td>
      <td>4</td>
    </tr>
    <tr>
      <th>11</th>
      <td>경북</td>
      <td>4</td>
    </tr>
    <tr>
      <th>12</th>
      <td>대전</td>
      <td>3</td>
    </tr>
    <tr>
      <th>13</th>
      <td>제주</td>
      <td>3</td>
    </tr>
    <tr>
      <th>14</th>
      <td>강원</td>
      <td>2</td>
    </tr>
    <tr>
      <th>15</th>
      <td>충북</td>
      <td>2</td>
    </tr>
    <tr>
      <th>16</th>
      <td>전북</td>
      <td>2</td>
    </tr>
    <tr>
      <th>17</th>
      <td>울산</td>
      <td>2</td>
    </tr>
    <tr>
      <th>18</th>
      <td>경기북부</td>
      <td>1</td>
    </tr>
    <tr>
      <th>19</th>
      <td>강원</td>
      <td>1</td>
    </tr>
    <tr>
      <th>20</th>
      <td>경기</td>
      <td>3</td>
    </tr>
    <tr>
      <th>21</th>
      <td>경북</td>
      <td>2</td>
    </tr>
    <tr>
      <th>22</th>
      <td>경상</td>
      <td>1</td>
    </tr>
    <tr>
      <th>23</th>
      <td>광주</td>
      <td>1</td>
    </tr>
    <tr>
      <th>24</th>
      <td>대구</td>
      <td>1</td>
    </tr>
    <tr>
      <th>25</th>
      <td>부산</td>
      <td>5</td>
    </tr>
    <tr>
      <th>26</th>
      <td>울산</td>
      <td>1</td>
    </tr>
    <tr>
      <th>27</th>
      <td>전남</td>
      <td>4</td>
    </tr>
    <tr>
      <th>28</th>
      <td>전북</td>
      <td>3</td>
    </tr>
    <tr>
      <th>29</th>
      <td>충남</td>
      <td>1</td>
    </tr>
    <tr>
      <th>30</th>
      <td>충북</td>
      <td>4</td>
    </tr>
  </tbody>
</table>
</div>




```python
df3=df3.groupby('지사명')['시설수'].sum()  # 지사명, 시설수 합 그룹핑
```


```python
df3
```




    지사명
    강원       3
    경기       9
    경기북부     1
    경남       4
    경북       6
    경상       1
    광주       5
    대구       7
    대전       3
    부산      17
    서울      12
    서울남부    12
    서울동부     7
    울산       3
    인천       5
    전남       9
    전북       5
    제주       3
    충남       5
    충북       6
    Name: 시설수, dtype: int64




```python
df3=pd.DataFrame(df3)  #df3 데이터프레임 생성
```


```python
df3
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>시설수</th>
    </tr>
    <tr>
      <th>지사명</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>강원</th>
      <td>3</td>
    </tr>
    <tr>
      <th>경기</th>
      <td>9</td>
    </tr>
    <tr>
      <th>경기북부</th>
      <td>1</td>
    </tr>
    <tr>
      <th>경남</th>
      <td>4</td>
    </tr>
    <tr>
      <th>경북</th>
      <td>6</td>
    </tr>
    <tr>
      <th>경상</th>
      <td>1</td>
    </tr>
    <tr>
      <th>광주</th>
      <td>5</td>
    </tr>
    <tr>
      <th>대구</th>
      <td>7</td>
    </tr>
    <tr>
      <th>대전</th>
      <td>3</td>
    </tr>
    <tr>
      <th>부산</th>
      <td>17</td>
    </tr>
    <tr>
      <th>서울</th>
      <td>12</td>
    </tr>
    <tr>
      <th>서울남부</th>
      <td>12</td>
    </tr>
    <tr>
      <th>서울동부</th>
      <td>7</td>
    </tr>
    <tr>
      <th>울산</th>
      <td>3</td>
    </tr>
    <tr>
      <th>인천</th>
      <td>5</td>
    </tr>
    <tr>
      <th>전남</th>
      <td>9</td>
    </tr>
    <tr>
      <th>전북</th>
      <td>5</td>
    </tr>
    <tr>
      <th>제주</th>
      <td>3</td>
    </tr>
    <tr>
      <th>충남</th>
      <td>5</td>
    </tr>
    <tr>
      <th>충북</th>
      <td>6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df3.columns
```




    Index(['시설수'], dtype='object')




```python
df3['시설수'].sum()  #총 시설 개수 
```




    123




```python
df3.rename(columns={'시설수':'시설수(총:123)'},inplace=True)
```


```python
from matplotlib.pyplot import figure
df3.plot(kind='bar', title='장애인 직업교육기관 현황(2017)',figsize=(15,10))
```




    <matplotlib.axes._subplots.AxesSubplot at 0x28ffb0d10f0>




![png](output_36_1.png)



```python
df4
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>경험 여부별</th>
      <th>경제활동상태별</th>
      <th>2015</th>
      <th>2015.1</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>경험 여부별</td>
      <td>경제활동상태별</td>
      <td>추정수 (명)</td>
      <td>비율 (%)</td>
    </tr>
    <tr>
      <th>1</th>
      <td>계</td>
      <td>계</td>
      <td>2210499</td>
      <td>100</td>
    </tr>
    <tr>
      <th>2</th>
      <td>NaN</td>
      <td>취업자</td>
      <td>904140</td>
      <td>100</td>
    </tr>
    <tr>
      <th>3</th>
      <td>NaN</td>
      <td>실업자</td>
      <td>46597</td>
      <td>100</td>
    </tr>
    <tr>
      <th>4</th>
      <td>NaN</td>
      <td>비경제활동인구</td>
      <td>1259762</td>
      <td>100</td>
    </tr>
    <tr>
      <th>5</th>
      <td>경험 있음</td>
      <td>계</td>
      <td>185682</td>
      <td>8.4</td>
    </tr>
    <tr>
      <th>6</th>
      <td>NaN</td>
      <td>취업자</td>
      <td>109401</td>
      <td>12.1</td>
    </tr>
    <tr>
      <th>7</th>
      <td>NaN</td>
      <td>실업자</td>
      <td>6896</td>
      <td>14.8</td>
    </tr>
    <tr>
      <th>8</th>
      <td>NaN</td>
      <td>비경제활동인구</td>
      <td>69287</td>
      <td>5.5</td>
    </tr>
    <tr>
      <th>9</th>
      <td>경험 없음</td>
      <td>계</td>
      <td>2024817</td>
      <td>91.6</td>
    </tr>
    <tr>
      <th>10</th>
      <td>NaN</td>
      <td>취업자</td>
      <td>794739</td>
      <td>87.9</td>
    </tr>
    <tr>
      <th>11</th>
      <td>NaN</td>
      <td>실업자</td>
      <td>39701</td>
      <td>85.2</td>
    </tr>
    <tr>
      <th>12</th>
      <td>NaN</td>
      <td>비경제활동인구</td>
      <td>1190475</td>
      <td>94.5</td>
    </tr>
  </tbody>
</table>
</div>




```python
df4=df4.T[[5,9]]  # 경험여부만 사용하기 위해 반전시킨후 5,9 열을 가져옴
```


```python
df4
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>5</th>
      <th>9</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>경험 여부별</th>
      <td>경험 있음</td>
      <td>경험 없음</td>
    </tr>
    <tr>
      <th>경제활동상태별</th>
      <td>계</td>
      <td>계</td>
    </tr>
    <tr>
      <th>2015</th>
      <td>185682</td>
      <td>2024817</td>
    </tr>
    <tr>
      <th>2015.1</th>
      <td>8.4</td>
      <td>91.6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df4=df4.T    # 원래대로 다시 반전
```


```python
df4
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>경험 여부별</th>
      <th>경제활동상태별</th>
      <th>2015</th>
      <th>2015.1</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5</th>
      <td>경험 있음</td>
      <td>계</td>
      <td>185682</td>
      <td>8.4</td>
    </tr>
    <tr>
      <th>9</th>
      <td>경험 없음</td>
      <td>계</td>
      <td>2024817</td>
      <td>91.6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df4=df4[['경험 여부별','2015.1']]  # 필요한 정보 추출
```


```python
df4
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>경험 여부별</th>
      <th>2015.1</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5</th>
      <td>경험 있음</td>
      <td>8.4</td>
    </tr>
    <tr>
      <th>9</th>
      <td>경험 없음</td>
      <td>91.6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df4=df4.rename(columns={'2015.1':'비율'}) # 2015.1 을 비율로 이름바꿈
```


```python
df4
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>경험 여부별</th>
      <th>비율</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5</th>
      <td>경험 있음</td>
      <td>8.4</td>
    </tr>
    <tr>
      <th>9</th>
      <td>경험 없음</td>
      <td>91.6</td>
    </tr>
  </tbody>
</table>
</div>




```python
df4=pd.DataFrame(df4.groupby('경험 여부별')['비율'].sum()) # 경험 여부별, 비율의 합을 그룹핑하고 데이타프레임으로 만들어 df4 에 저장
```


```python
df4
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>비율</th>
    </tr>
    <tr>
      <th>경험 여부별</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>경험 없음</th>
      <td>91.6</td>
    </tr>
    <tr>
      <th>경험 있음</th>
      <td>8.4</td>
    </tr>
  </tbody>
</table>
</div>




```python
df4=df4.reset_index() # 인덱스 리셋
```


```python
df4
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>경험 여부별</th>
      <th>비율</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>경험 없음</td>
      <td>91.6</td>
    </tr>
    <tr>
      <th>1</th>
      <td>경험 있음</td>
      <td>8.4</td>
    </tr>
  </tbody>
</table>
</div>




```python
import matplotlib.pyplot as plt
# pie 그래프
plt.rcParams['figure.figsize'] = [12, 8]   # 사이즈 설정
plt.pie(df4['비율'],   

        explode=(0.1,0), # 첫 번째 파이가 튀어나오게 설정

        labels=df4['경험 여부별'], 

        colors=['yellowgreen','lightskyblue'], 

        autopct='%1.2f%%', # second decimal place

        shadow=True, 

        startangle=90, #어디에서 시작할지 정함

        textprops={'fontsize': 14}) # text font size

plt.axis('equal') #  equal length of X and Y axis

plt.title('직업교육여부', fontsize=20)

plt.show()
```


![png](output_50_0.png)

